<?php

use Illuminate\Database\Seeder;
use App\OvertimeRates;

class OvertimeSeeder extends Seeder
{
    public function run()
    {
		OvertimeRates::truncate();
		
        OvertimeRates::create(['id' => 1, 'name' => 'Avond tarief']);
        OvertimeRates::create(['id' => 2, 'name' => 'Nachttarief']);
        OvertimeRates::create(['id' => 3, 'name' => 'Meerwerk tarief']);
        OvertimeRates::create(['id' => 4, 'name' => 'Overwerk tarief']);
        OvertimeRates::create(['id' => 5, 'name' => 'Zaterdag tarief']);
        OvertimeRates::create(['id' => 6, 'name' => 'Zondag tarief']);
    }
}
