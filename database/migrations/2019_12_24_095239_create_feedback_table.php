<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->text('text');
            $table->integer('status')->default(0);
            $table->integer('type'); // 0 = feedback 1 = bug
        });
    }

    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
