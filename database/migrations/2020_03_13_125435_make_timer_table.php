<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTimerTable extends Migration
{
    public function up()
    {
        Schema::create('timers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('date');
            $table->string('time');            
        });
    }

    public function down()
    {
        Schema::dropIfExists('timers');
    }
}
