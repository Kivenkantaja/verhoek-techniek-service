<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDurationStringTimerNotes extends Migration
{
    public function up()
    {
        Schema::table('timer_notes', function (Blueprint $table) {
            $table->string('duration')->change();
        });
    }

    public function down()
    {
        Schema::table('timer_notes', function (Blueprint $table) {
            $table->integer('duration')->change();
        });
    }
}
