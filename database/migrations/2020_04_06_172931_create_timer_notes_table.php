<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimerNotesTable extends Migration
{
    public function up()
    {
        Schema::create('timer_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('user_id');
            $table->string('date');
            $table->string('time');
            $table->integer('duration');
        });
    }

    public function down()
    {
        Schema::dropIfExists('timer_notes');
    }
}
