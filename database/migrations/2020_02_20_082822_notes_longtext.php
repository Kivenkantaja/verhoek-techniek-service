<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotesLongtext extends Migration
{
    public function up()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->longtext('note')->change();
        });
    }

    public function down()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->string('note')->change();
        });
    }
}
