<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFeedback extends Migration
{
    public function up()
    {
        Schema::table('feedback', function (Blueprint $table) {
            $table->integer('estimate_type')->nullable();
            $table->string('estimate_hours')->nullable();
        });
    }

    public function down()
    {
        Schema::table('feedback', function (Blueprint $table) {
            $table->dropColumn('estimate_type');
            $table->dropColumn('estimate_hours');
        });
    }
}
