<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToProjectMaterials extends Migration
{
    public function up()
    {
        Schema::table('project_materials', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
    }

    public function down()
    {
        Schema::table('project_materials', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
