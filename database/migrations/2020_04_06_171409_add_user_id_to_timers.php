<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToTimers extends Migration
{
    public function up()
    {
        Schema::table('timers', function (Blueprint $table) {
            $table->integer('user_id');
        });
    }

    public function down()
    {
        Schema::table('timers', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
