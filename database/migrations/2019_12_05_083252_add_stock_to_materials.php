<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStockToMaterials extends Migration
{
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->integer('stock')->default(0);
        });

        Schema::create('stock_mutations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('deleted_at');
            $table->integer('material_id');
            $table->integer('mutation');
        });
    }

    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('stock');
        });

        Schema::dropIfExists('stock_mutations');
    }
}
