<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOvertimeFields extends Migration
{
    public function up()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->string('overtime_rate_id')->nullable();
            $table->string('overtime_percentage')->nullable();
        });

        Schema::create('overtime_rates', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->dropColumn('rate_id');
            $table->dropColumn('rate_percentage');
        });

        Schema::dropIfExists('overtime_rates');
    }
}
