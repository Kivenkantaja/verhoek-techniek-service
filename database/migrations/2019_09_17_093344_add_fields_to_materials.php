<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToMaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->string('number')->nullable();
            $table->string('price')->nullable();
            $table->string('description')->nullable();
            $table->string('buyPrice')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('number');
            $table->dropColumn('price');
            $table->dropColumn('description');
            $table->dropColumn('buyPrice');            
        });
    }
}
