<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectIdToRates extends Migration
{
    public function up()
    {
        Schema::table('rates', function (Blueprint $table) {
            $table->integer('project_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('rates', function (Blueprint $table) {
            $table->dropColumn('project_id');
        });
    }
}
