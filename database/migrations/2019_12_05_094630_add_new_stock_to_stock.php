<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewStockToStock extends Migration
{
    public function up()
    {
        Schema::table('stock_mutations', function (Blueprint $table) {
            $table->integer('new_stock');
            $table->string('deleted_at')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('stock_mutations', function (Blueprint $table) {
            $table->dropColumn('new_stock');
            $table->string('deleted_at')->change();
        });


    }
}
