<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('user_id');
            $table->integer('client_id');
            $table->softDeletes();            
        });

        Schema::create('hours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('minutes');
            $table->integer('user_id');
            $table->integer('client_id');
            $table->string('date');
            $table->softDeletes();
        });

        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->string('adress')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('kvk')->nullable();
            $table->string('phone')->nullable();
        });       

        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('client_id');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
        });                        
    }

    public function down()
    {
        Schema::dropIfExists('projects');
        Schema::dropIfExists('hours');
        Schema::dropIfExists('clients');
        Schema::dropIfExists('contacts');
    }
}
