<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToTimers extends Migration
{
    public function up()
    {
        Schema::table('timers', function (Blueprint $table) {
            $table->longText('description')->nullable();
        });

        Schema::table('timer_notes', function (Blueprint $table) {
            $table->longText('text');
        });
    }

    public function down()
    {
        Schema::table('timers', function (Blueprint $table) {
            $table->dropColumn('description');
        });

        Schema::table('timer_notes', function (Blueprint $table) {
            $table->dropColumn('text');
        });        
    }
}
