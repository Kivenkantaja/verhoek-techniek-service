<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDateNullableInHours extends Migration
{
    public function up()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->string('date')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->string('date')->change();
        });
    }
}
