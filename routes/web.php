<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register' => false]);
Route::group(['middleware' => 'auth'], function() {
	//Hours
	Route::get('uren', 'HourController@index');
	Route::post('uren/opslaan', 'HourController@save');
	Route::get('uren/verwijderen/{id}', 'HourController@delete');
	Route::post('updateDesc', 'HourController@updateDesc');

	//api routes
	Route::get('getProjects', 'ProjectController@getProjects');
	Route::get('finishProject/{id}', 'ProjectController@finishProject');
	Route::get('getMode', 'HomeController@getMode');
	Route::get('projectMaterials', 'ProjectController@projectMaterials');
	Route::get('lastUsedProject', 'ProjectController@lastUsedProject');

	Route::get('getClient/{id}', 'ClientController@getClient');
	Route::get('getClients', 'ClientController@getClients');
	
	Route::get('getHours', 'HourController@hours');	
	Route::get('getHour/{id}', 'HourController@getHour');
	Route::get('queryHours', 'HourController@queryHours');
	Route::get('getHourList', 'ProjectController@getHourList');

	Route::get('minStock', 'HomeController@minStock');
	Route::get('getMaterial/{id}', 'MaterialController@getMaterial');
	Route::get('getMaterials', 'MaterialController@getMaterials');
	Route::get('getUser', 'UserController@getUser');

	Route::get('projecten', 'ProjectController@index');
	Route::get('projecten/inzien/{id}', 'ProjectController@view');
	Route::post('projecten/{project}/upload', 'ProjectPhotoController@uploadPhoto');
	Route::post('projecten/updatePhoto/{projectPhoto}', 'ProjectPhotoController@updatePhoto');
	Route::get('projecten/{project}/photos', 'ProjectPhotoController@getPhotos');

	Route::post('linkMaterials', 'MaterialController@linkMaterials');
	Route::post('updateLink/{material}', 'MaterialController@updateLink');
	Route::post('editLink', 'MaterialController@editLink');	
	Route::get('unlinkMaterial/{id}', 'MaterialController@unlinkMaterial');
	Route::get('getProjectMaterial/{material}', 'MaterialController@getProjectMaterial');

	Route::resource('timers', 'TimerController');
	Route::resource('timernotes', 'TimerNoteController');
	Route::get('getTimerNotes', 'TimerNoteController@getTimerNotes');

	Route::get('loguit', 'Auth\LoginController@logout');		

	Route::resource('notes', 'NoteController');
	Route::get('getNotes/{project_id}', 'NoteController@getNotes');

	Route::group(['middleware' => 'admin'], function() {
		Route::get('/', 'HomeController@index');

		//Clients
		Route::get('klanten', 'ClientController@index');
		Route::get('klanten/inzien/{id}', 'ClientController@view');
		Route::get('klanten/bewerken/{id}', 'ClientController@edit');
		Route::get('klanten/verwijderen/{id}', 'ClientController@delete');
		Route::get('klanten/nieuw', 'ClientController@make');
		Route::post('klanten/opslaan', 'ClientController@save');

		//Projects
		Route::get('projecten/bewerken/{id}', 'ProjectController@edit');
		Route::get('projecten/verwijderen/{id}', 'ProjectController@delete');
		Route::get('projecten/nieuw', 'ProjectController@make');
		Route::get('projecten/nieuw/{id}', 'ProjectController@make');
		Route::post('projecten/opslaan', 'ProjectController@save');	

		Route::get('/finishProject/{id}', 'ProjectController@finish');
		Route::get('/restoreProject/{id}', 'ProjectController@restore');

		Route::post('/saveInvoice', 'ProjectController@invoice');

		//Users
		Route::get('gebruikers', 'UserController@index');
		Route::get('gebruikers/inzien/{id}', 'UserController@view');
		Route::get('gebruikers/bewerken/{id}', 'UserController@edit');
		Route::get('gebruikers/verwijderen/{id}', 'UserController@delete');
		Route::get('gebruikers/nieuw', 'UserController@make');
		Route::post('gebruikers/opslaan', 'UserController@save');

		Route::get('toggleUser/{id}', 'UserController@toggleUser');
		Route::get('getUsers', 'UserController@getUsers');

		Route::get('pdf', 'ProjectController@pdf');

		Route::get('getRate', 'RateController@getRate');
		Route::post('saveRate', 'RateController@saveRate');
		Route::delete('deleteRate/{rate}', 'RateController@delete');

		Route::resource('feedback', 'FeedbackController');
		Route::get('getFeedback', 'FeedbackController@getFeedback');
		Route::get('confirmFeedback/{id}', 'FeedbackController@confirm');
		Route::get('deleteFeedback', 'FeedbackController@remove');

		Route::get('getActivities/{user}', 'ActivityController@getActivities');

		Route::get('getOvertimeRates', 'HourController@getOvertimeRates');
	});
	
	Route::group(['middleware' => 'accessMaterials'], function () {
		//Materials
		Route::get('materialen', 'MaterialController@index');
		Route::get('materialen/inzien/{id}', 'MaterialController@view');
		Route::get('materialen/bewerken/{id}', 'MaterialController@edit');
		Route::get('materialen/verwijderen/{id}', 'MaterialController@delete');
		Route::get('materialen/nieuw', 'MaterialController@make');
		Route::post('materialen/opslaan', 'MaterialController@save');
		Route::post('materials/upload/{material}', 'MaterialController@uploadPhoto');
		Route::delete('materials/deletePhoto/{material}', 'MaterialController@deletePhoto');
		Route::post('updateStock', 'MaterialController@updateStock');
		Route::get('getStockValue', 'MaterialController@getStockValue');
	});

	Route::get('costs', 'CostController@index');
	Route::get('costsProjects', 'CostController@projects');

	Route::post('search', 'SearchController@search');
});
