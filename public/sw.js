self.addEventListener('fetch', function(event) {
    caches.keys().then(function(names) {
        for (let name of names)
            caches.delete(name);
    });
})

// Clear cache on activate
self.addEventListener('activate', event => {
	event.waitUntil(
		caches.keys().then(cacheNames => {
			return Promise.all(
				cacheNames
					.filter(cacheName => (cacheName.startsWith("pwa-")))
					.filter(cacheName => (cacheName !== staticCacheName))
					.map(cacheName => caches.delete(cacheName))
			);
		})
	);
});