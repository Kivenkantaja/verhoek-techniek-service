<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'name',
    	'adress',
    	'postal_code',
    	'town',
    	'kvk',
    	'phone',
        'email',
        'rate'
    ];

    public function projects()
    {
    	return $this->hasMany('App\Project');
    }    

    public function rates()
    {
        return $this->hasMany('App\Rate');
    }
}
