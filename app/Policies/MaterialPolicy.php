<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MaterialPolicy
{
    use HandlesAuthorization;

	/**
	 * @param User $user
	 * 
	 * @return bool
	 */
	public function see(User $user)
	{
		return (user()->permissions->seeMaterials ?? false) || user()->user_type === 1;
	}

	/**
	 * @param User $user
	 * 
	 * @return bool
	 */
	public function edit(User $user)
	{
		return (user()->permissions->editMaterials ?? false) || user()->user_type === 1;
	}
}
