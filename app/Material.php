<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{
	use SoftDeletes;

	public $timestamps = false;

    protected $fillable = [
    	'name',
        'unit',
        'number',
		'type_number',
		'ean_number',
        'price',
        'description',
        'buyPrice',
        'minimum_stock',
        'photo'
    ];

    public function projectMaterial()
    {
    	return $this->hasMany('App\projectMaterial');
    }

    public function mutations()
    {
    	return $this->hasMany('App\Stock');
    }
}
