<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
    	'client_id',
    	'user_id',
        'project_id', 
    	'rate'
    ];

    public function client()
    {
    	return $this->belongsTo('App\Client');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
