<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	protected $fillable = [
		'seeMaterials',
		'editMaterials',
	];
	
    public function user()
	{
		$this->belongsTo(User::class);
	}
}
