<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OvertimeRates extends Model
{
    protected $fillable = ['name'];
}
