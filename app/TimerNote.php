<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimerNote extends Model
{
    protected $fillable = [
    	'user_id', 'date', 'time', 'duration', 'text'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
