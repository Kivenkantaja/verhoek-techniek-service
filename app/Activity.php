<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
    	'user_id', 'project_id', 'type', 'action', 'data'
    ];

    public function project() 
    {
    	return $this->belongsTo('App\Project');
    }
 }
