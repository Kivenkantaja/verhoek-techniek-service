<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhoto extends Model
{
    protected $fillable = [
    	'project_id', 'path', 'description',	
	];
    
    public function project()
	{
		return $this->belongsTo(Project::class);
	}
}
