<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    protected $fillable = [
    	'project_id',
        'user_id',
    	'minutes',
    	'date',
        'description',
		'status',
        'overtime_rate_id',
        'overtime_percentage',
		'show_on_report',
    ];

    public function project()
    {
    	return $this->belongsTo('App\Project');
    }

    public function client()
    {
    	return $this->belongsTo('App\Client');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function overtime_rate()
    {
        return $this->belongsTo('App\OvertimeRates');
    }

    public function format($minutes)
    {
        $hours = (int) ($minutes / 60);
        $minutes = sprintf('%02d', $minutes % 60);
        $output = "$hours:$minutes";
        return $output;
    }
}
