<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        'App\Events\ChangedStock' => [
            'App\Listeners\UpdateStock',
            'App\Listeners\AddMutation'
        ],
        'App\Events\Action' => [
            'App\Listeners\SaveAction'
        ],
    ];
	
    public function boot()
    {
        parent::boot();

        //
    }
}
