<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timer extends Model
{
    protected $fillable = [
    	'date', 'time', 'user_id', 'description'
    ];
}
