<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password', 'rate'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hours()
    {
        return $this->hasMany('App\Hour');
    }

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function rates()
    {
        return $this->hasMany('App\Rate');
    }    

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }
	
	public function permissions()
	{
		return $this->hasOne(Permission::class);
	}
	
	public function getIsAdminAttribute(): bool
	{
		return $this->user_type === 'admin';
	}

	public function getIsClientAttribute(): bool
	{
		return $this->user_type === 'client';
	}

	public function getIsEmployeeAttribute(): bool
	{
		return $this->user_type === 'employee';
	}
}
