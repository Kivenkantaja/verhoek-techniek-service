<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock_mutations';

    public function material()
    {
    	return $this->belongsTo('App\Material');
    }
}
