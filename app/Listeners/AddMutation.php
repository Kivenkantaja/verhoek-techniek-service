<?php

namespace App\Listeners;

use App\Events\ChangedStock;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Stock;

class AddMutation
{
    public function handle(ChangedStock $event)
    {
        $stock = new Stock;
        $stock->mutation = $event->data->mutation;
        $stock->material_id = $event->data->material->id;
        $stock->new_stock = $event->data->material->stock;
        $stock->save();        
    }
}
