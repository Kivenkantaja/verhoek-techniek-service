<?php

namespace App\Listeners;

use App\Events\Action;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Activity;

class SaveAction
{
    public function handle(Action $event)
    {
        Activity::create($event);
    }
}
