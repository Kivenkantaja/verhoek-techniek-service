<?php

namespace App\Listeners;

use App\Events\ChangedStock;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateStock
{
    public function handle(ChangedStock $event)
    {
        $material = $event->data->material;
        $material->stock = $material->stock + $event->data->mutation;
        $material->save();
    }
}
