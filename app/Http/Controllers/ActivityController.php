<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use App\User;

class ActivityController extends Controller
{
    public function getActivities($user)
    {
    	return response()->json(Activity::where('user_id', $user)->orderBy('id', 'desc')->with('project')->get());
    }
}
