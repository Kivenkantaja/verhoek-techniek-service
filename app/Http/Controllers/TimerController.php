<?php

namespace App\Http\Controllers;

use App\Timer;
use Illuminate\Http\Request;
use Auth;

class TimerController extends Controller
{
    public function index(Request $request)
    {
        Timer::where('user_id', $request->user_id)->where('date', '!=', date('Y-m-d'))->delete();

        return json_encode(Timer::where('user_id', $request->user_id)->first());
    }

    public function store(Request $request)
    {
        Timer::create($request->input());

        return json_Encode(Timer::first());
    }

    public function update(Request $request, Timer $timer) 
    {
        $timer->update($request->input());
    }

    public function destroy(Timer $timer)
    {
        $timer->delete();
    }
}
