<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Material;
use App\ProjectMaterial;
use App\Project;
use App\Stock;
use App\Activity;

use Auth;

use App\Events\ChangedStock;

class MaterialController extends Controller
{
    public function index()
    {
    	return view('materials.index');
    }

    public function view($id)
    {
        $material = Material::where('id', $id)->first();

        $links = ProjectMaterial::where('material_id', $id)->get();

        $project_ids = array();
        foreach($links as $link)
        {
            $project_ids[] = $link->project_id;
        }

        $projects = Project::whereIn('id', array_unique($project_ids))->get();

        return view('materials.view', compact('material', 'projects'));
    }

    public function edit($id)
    {
        $material = Material::where('id', $id)->first();

        return view('materials.edit', compact('material'));        
    }

    public function make()
    {
    	return view('materials.edit');
    }

    public function save(Request $request)
    {
        if($request->id) {
            $material = Material::where('id', $request->id)->first();
        } else {
            $material = new Material;
        }

		$material->timestamps = true;

        $data = $request->input();

        if(strpos($data['price'], ','))
        {
            $data['price'] = explode(',', $data['price']);
            $data['price'] = implode('.', $data['price']);
        }

        if(strpos($data['buyPrice'], ','))
        {
            $data['buyPrice'] = explode(',', $data['buyPrice']);
            $data['buyPrice'] = implode('.', $data['buyPrice']);
        }

    	$material->fill($data);
    	$material->save();

        if(!$request->request_type) {
    	   return redirect('/materialen');
        } else {
            return json_encode((object) [
                'status' => 1,
                'message' => 'Materiaal succesvol opgeslagen'
            ]);
        }
    }

    public function delete($id)
    {
        Material::where('id', $id)->first()->delete();

        return redirect('/materialen');
    }

    public function getMaterial($id)
    {
        $material = Material::where('id', $id)->first();

        return json_encode($material);
    }

    public function getMaterials(Request $request)
    {
        if(!$request->search) {
            $materials = Material::orderBy('name')->get();
        } else {
            $terms = explode(' ', $request->search);

            $materials = Material::orderBy('name')->where(function($query) use ($terms) {
                foreach($terms as $term) {
                    $query->where('name', 'like', "%$term%");
                }
            })->orWhere(function($query) use ($terms) {
                foreach($terms as $term) {
                    $query->where('number', 'like', "%$term%");
                }
            })->orWhere(function($query) use ($terms) {
                foreach($terms as $term) {
                    $query->where('description', 'like', "%$term%");
                }
            })->get();
        }

        foreach($materials as $key => $material)
        {
            $materials[$key]->parsedPrice = number_format($material->price, 2, ',', '');
            if($material->buyPrice)
            {
                $materials[$key]->parsedBuyPrice = number_format($material->buyPrice, 2, ',', '');
            }

            $stock = $material->stock > 0 ? $material->stock : 0;
            $value = $stock * $material->buyPrice;
            $materials[$key]->value = $value;
            $materials[$key]->parsedValue = number_format($value, 2, ',', '');
        }

        return json_encode($materials);
    }

    public function linkMaterials(Request $request)
    {
        $link = new ProjectMaterial;
        $link->fill($request->input());
        $link->save();

        $material = Material::where('id', $request->material_id)->first();

        if(!$material) {
            $material = $link;
        }

        $data = (object) [
            'mutation' => 0 - $request->amount,
            'material' => $material
        ];

        Activity::create([
            'type' => 'materials',
            'user_id' => Auth::user()->id,
            'project_id' => $request->project_id,
            'action' => 'new',
            'data' => json_encode([
                'name' => $material->name,
                'unit' => $material->unit,
                'amount' => $link->amount
            ])
        ]);

        if($request->material_id) {
            event(new ChangedStock($data));
        }

        return json_encode((object) [
            'status' => 1,
            'message' => 'Materialen succesvol toegevoegd.'
        ]);
    }

    public function updateLink(ProjectMaterial $material, Request $request) 
    {
        $material->update($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Materialen succesvol bijgewerkt.'
        ]);
    }

    public function unlinkMaterial($id)
    {
        $link = ProjectMaterial::where('id', $id)->first();
        $id = $link->project_id;
        $link->delete();

        return json_encode((object) [
            'status' => 1,
            'message' => 'Materialen succesvol verwijderd.'
        ]);
    }

    public function editLink(Request $request)
    {
        $link = ProjectMaterial::where('id', $request->id)->first();   

        $mutation = $link->amount - $request->amount;

        if($mutation < 0 && $link->status == 1 || $mutation < 0 && $link->date != date('Y-m-d')) {
            $link = $link->toArray();
            $link['amount'] = 1;
            $link['status'] = 0;
            $link['date'] = date('Y-m-d');

            $link = ProjectMaterial::create($link);
        } else {
            $link->fill($request->input());

            if($link->amount < 1) {
                $link->delete();            
            } else {
                $link->save();
            }
        }
        
        $data = (object) [
            'material' => Material::where('id', $link->material_id)->first(),
            'mutation' => $mutation
        ];
        
        event(new ChangedStock($data));
        
        return json_encode((object) [
            'status' => 1,
            'message' => 'Materialen succesvol aangepast'
        ]);
    }

    public function updateStock(Request $request)
    {
        $data = (object) [
            'material' => Material::where('id', $request->material['id'])->first(),
            'mutation' => $request->mutation
        ];

        event(new ChangedStock($data));

        return json_encode((object) [
            'material' => Material::where('id', $request->material['id'])->first(),
            'message' => (object) [
                'message' => 'Voorraad succesvol bijgewerkt!',
                'status' => 1,
            ]
        ]);
    }

    public function getStockValue() 
    {
        $materials = Material::get();

        $array = [];
        foreach($materials as $material)
        {
            $array[] = $material->stock > 0 ? $material->buyPrice * $material->stock : 0;
        }

        return number_format(array_sum($array), 2, ',', '.');
    }

    public function getProjectMaterial(ProjectMaterial $material)
    {
        return json_encode($material);
    }

    public function uploadPhoto(Request $request, Material $material)
    {
        $result = $request->file('file')->store('photos/materials', 'public');

        $material->update(['photo' => $result]);
    }

    public function deletePhoto(Material $material)
    {
        $result = Storage::disk('public')->delete($material->photo);

        $material->update(['photo' => null]);

        return response()->json($material);
    }
}
