<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index()
    {
        return view('feedback.index');
    }

    public function getFeedback(Request $request)
    {
        $feedback = Feedback::where('status', $request->status)->get();

        foreach($feedback as $key => $item)
        {
            $feedback[$key]->date = date('d-m-Y', strtotime($item->created_at));
        }

        return json_encode($feedback);
    }

    public function store(Request $request)
    {
        $feedback = new Feedback;
        $feedback->fill($request->input());
        $feedback->save();

        return json_encode((object) [
            'status' => 1,
            'message' => 'Feedback succesvol opgeslagen.'
        ]);
    }

    public function confirm($id) 
    {
        Feedback::where('id', $id)->update(['status' => 1]);

        return json_encode((object) [
            'status' => 1,
            'message' => 'Status bijgewerkt.'
        ]);
    }

    public function update(Request $request, Feedback $feedback)
    {
        //
    }

    public function destroy(Feedback $feedback)
    {
        //
    }

    public function remove(Request $request)
    {
        Feedback::destroy($request->id);

        return json_encode((object) [
            'status' => 1,
            'message' => 'Feedback verwijderd.'
        ]);
    }
}
