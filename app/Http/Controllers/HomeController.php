<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Material;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $projects = Project::whereHas('hours', function($query) {
            $query->where('status', 0);
            $query->where('date', '!=', null);
        })->with(['hours' => function($query) {
            $query->where('date', '!=', null);
            $query->where('status', 0);
            $query->orderBy('date');
        }])->get();

        $materials = Material::whereHas('mutations')->whereRaw('stock < minimum_stock')->get();

        $data = array();

        foreach($projects as $project)
        {
            $data[] = (object) [
                'date' => $project->hours[0]->date,
                'project' => Project::where('id', $project->hours[0]->project_id)->with(['client'])->first(),
            ];
        }         

        usort($data, 'sortByDate');   

        return view('home', compact('data', 'materials'));
    }

    public function minStock()
    {
        $materials = Material::whereHas('mutations')
                             ->whereRaw('stock < minimum_stock')
                             ->get(); 

        return json_encode($materials);        
    }

    public function getMode()
    {
        return Auth::user()->user_type;
    }
}
