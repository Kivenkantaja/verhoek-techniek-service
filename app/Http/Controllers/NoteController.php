<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

use Auth;

class NoteController extends Controller
{
    public function getNotes($project_id) {
        $notes = Note::where('project_id', $project_id);

        // if(Auth::user()->user_type == 0) {
        //     $notes = $notes->where('user_id', Auth::user()->id);
        // }

        return json_encode($notes->get());
    }

    public function store(Request $request)
    {
        Note::create($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Notitie opgeslagen'
        ]);
    }

    public function update(Request $request, Note $note)
    {
        $note->update($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Notitie bijgewerkt'
        ]);
    }

    public function destroy(Note $note)
    {
        $note->delete();

        return json_encode([
            'status' => 1,
            'message' => 'Notitie verwijderd.'
        ]);
    }
}
