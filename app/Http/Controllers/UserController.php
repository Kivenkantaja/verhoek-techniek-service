<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Rate;
use Auth;

class UserController extends Controller
{
    public function index()
    {
    	return view('users.index');
    }

    public function view($id)
    {
    	$user = User::where('id', $id)->first();

    	return view('users.view', compact('user'));
    }

    public function getUsers(Request $request)
    {
        $client = $request->client ? $request->client : null;
        $project = $request->project ? $request->project : null;

    	$data = (object) [
    		'active' => $this->queryUsers(1, $client, $project),
    		'disabled' => $this->queryUsers(0, $client, $project)
    	];

        return json_encode($data);
    }

    private function queryUsers($status, $client, $project)
    {
        $users = User::where('status', $status)->get();

        if($project) {
            $users = getRate($users, $client, $project, true);
        } else if($client) {
            $users = getRate($users, $client, null);
        }

        return $users;
    }

    public function getUser()
    {
        return json_encode(Auth::user());
    }

    public function make()
    {

    	return view('users.edit');
    }

    public function edit($id)
    {
    	$user = User::where('id', $id)->first();

    	return view('users.edit', compact('user'));
    }

    public function save(Request $request)
    {
    	if($request->id) {
    		$user = User::where('id', $request->id)->first();
    	} else {
    		$user = new User;
	    	$user->password = 0;
	    	$user->status = 0;    		
    	}

    	$user->fill($request->input());
    	$user->save();

    	return redirect('/gebruikers');
    }

    public function toggleUser($id)
    {
    	$user = User::where('id', $id)->first();

    	if($user->status == 1) {
    		$user->status = 0;
    	} else {
    		$user->status = 1;
    	}
    	$user->save();

    	return redirect("/gebruikers/inzien/$id");
    }
}
