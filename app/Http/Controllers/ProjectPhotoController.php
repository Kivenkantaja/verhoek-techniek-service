<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectPhoto;
use Illuminate\Http\Request;

class ProjectPhotoController extends Controller
{
	public function uploadPhoto(Request $request, $project): \Illuminate\Http\JsonResponse
	{
		$result = $request->file('file')->store('photos/projects/'.$project, 'public');

		$photo = ProjectPhoto::create([
			'project_id' => $project,
			'path' => $result,
		]);

		return response()->json([
			'data' => $photo,
			'status' => 200,
		]);
	}
	
	public function updatePhoto(Request $request, ProjectPhoto $projectPhoto): \Illuminate\Http\JsonResponse
	{
		$projectPhoto = $projectPhoto->update($request->input());
		
		return response()->json([
			'status' => 200,
			'data' => $projectPhoto,
		]);
	}
	
	public function getPhotos(Project $project): \Illuminate\Http\JsonResponse
	{
		return response()->json($project->photos()->get());	
	}
}
