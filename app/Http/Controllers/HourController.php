<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hour;
use App\Client;
use App\Project;
use App\User;
use App\Activity;
use App\OvertimeRates;
use Auth;

class HourController extends Controller
{
    public function index()
    {
    	return view('hours.index');
    }

    public function save(Request $request)
    {
        $data = $request->input();

        if($data['project_id'] == 0)
        {
            return json_encode((object) [
                'status' => 0,
                'message' => 'Je hebt geen project geselecteerd!'
            ]);
        }

        if(Auth::user()->user_type == 0)
        {
            $data['user_id'] = Auth::user()->id;
        }

    	if(!$request->id) {
            $hours = new Hour;
            $action = 'new';
        } else {
            $hours = Hour::where('id', $data['id'])->first();
            $action = 'edit';
        }

    	$hours->fill($data);
    	$hours->save();

        if(!$request->id) {
            Activity::create([
                'action' => $action,
                'user_id' => Auth::user()->id,
                'project_id' => $data['project_id'],
                'type' => 'hours',
                'data' => json_encode([
                    'minutes' => $hours->minutes
                ])
            ]);
        }

    	return json_encode((object) [
    		'status' => 1,
    		'message' => 'Uren zijn succesvol opgeslagen!',
    	]);
    }

    public function delete($id)
    {
        Hour::where('id', $id)->first()->delete();
        
        return json_encode([
            'status' => 1,
            'message' => 'Uren zijn verwijderd.'
        ]);
    }

    public function hours(Request $request) {
        $year = $request->year ? : date('Y');
        $weekNumber = $request->week ? $request->week : date('W');
        $week = weekByNumber($weekNumber, $year);
        $range = dateRange($week->first->date, $week->last->date);
        $user_ids = $request->user_ids ? $request->user_ids : [Auth::user()->id];
		
        $projects = Project::with(['user'])->whereHas('hours', function($query) use ($week, $user_ids) {
            $query->where('date', '>=', $week->first->date);
            $query->where('date', '<=', $week->last->date);
            if($user_ids) {
                $query->whereIn('user_id', $user_ids);
            }
        })->get();

        $allHours = array();
        foreach($projects as $project)
        {
            $rows = array();
            foreach($range as $day)
            {
                $data =  Hour::where('project_id', $project->id)->where('date', $day);

                if($user_ids)
                {
                    $data = $data->whereIn('user_id', $user_ids);
                }

                $data = $data->get();
                $hours = formatHours($data);

                $allHours[] = $data;

                $rows[] = (object) [
                    'date' => $day,
                    'data' => $data,
                    'hours' => $hours
                ];
            }     

            $output[] = [
                'period' => $rows,
                'project' => $project->name,
                'client' => $project->client ? $project->client->name : 'Verwijderde klant',
                'project_id' => $project->id
            ];
        }

        $weekTotal = $this->weekTotal($allHours);
        $headRow = $this->headRow($range);
        $footRow = $this->footRow($range, $user_ids);

        return json_encode((object) [
            'hourData' => isset($output) ? $output : null,
            'dateData' => $week,
            'headRow' => $headRow,
            'footRow' => $footRow,
            'total' => $weekTotal,
        ]);
    }

    private function weekTotal($hoursArray)
    {
        $minutes = array();
        foreach($hoursArray as $hours)
        {
            foreach($hours as $hour)
            {
                if($hour->minutes)
                {
                    $minutes[] = $hour->minutes;
                }
            }
        }

        $total = array_sum($minutes);
        $hours = (int) ($total / 60);
        $minutes = sprintf('%02d', $total % 60);
        $output = "$hours:$minutes";

        return $output;
    }

    private function headRow($range)
    {
        $output = array();

        foreach($range as $date)
        {
            $output[] = (object) [
                'date' => date('Y-m-d', strtotime($date)),
                'weekDay' => weekDays()[date('N', strtotime($date))],
                'day' => date('d', strtotime($date))
            ];
        }

        return $output;
    }

    private function footRow($range, $user_ids)
    {

        $data = array();
        foreach($range as $day)
        {
            $hours = Hour::where('date', $day)->whereIn('user_id', $user_ids)->get();

            $array = array();
            foreach($hours as $hour)
            {
                $array[] = $hour->minutes;
            }

            $total = outputHours(array_sum($array));

            $data[] = (object) [
                'date' => $day,
                'output' => $total
            ];
        }

        return $data;
    }

    public function queryHours(Request $request)
    {
        if(Auth::user()->user_type == 0)
        {
            $user_ids = [Auth::user()->id];
        } else {
            $user_ids = json_decode($request->user_ids);
        }

        $hours = Hour::where('date', $request->date)->with(['user'])
                     ->where('project_id', $request->project)
                     ->whereIn('user_id', $user_ids)
                     ->get();
        
        $project = Project::where('id', $request->project)->first();

        foreach($hours as $key => $hour)
        {
            $hours[$key]->output = $hour->format($hour->minutes);
        }

        return json_encode((object) [
            'hours' => $hours,
            'project' => $project
        ]);   
    }

    public function finishProject($id)
    {
        $project = Project::where('id', $id)->first();
        $project->status = 1;
        $project->save();

        return json_encode((object) [
            'status' => 1,
            'message' => 'Het project is afgerond.'
        ]);
    }

    public function invoiceProject($id)
    {
        Hour::where('project_id', $id)->update('status', 1);

        return json_encode((object) [
            'status' => 1,
            'message' => 'De uren zijn gemarkeerd als gefactureerd.'
        ]);
    }

    public function getUsers()
    {
        return json_encode(User::get());
    }

    public function getHour($id)
    {
        $hour = Hour::where('id', $id)->first();

        return json_encode($hour);
    }

    public function updateDesc(Request $request)
    {
        $project = $request->project_id;

        $hour = Hour::where('id', $request->id)->first();
        $hour->description = $request->description;
        $hour->save();

        return redirect("/projecten/inzien/$project");
    }

    public function getOvertimeRates()
    {
        return json_encode(OvertimeRates::get());
    }
}
