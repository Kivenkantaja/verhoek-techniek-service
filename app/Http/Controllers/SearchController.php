<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectMaterial;
use Illuminate\Http\Request;

class SearchController extends Controller
{
	public function search(Request $request) 
	{
		$projects = Project::where('name', 'like', '%' . $request->search . '%')->get();
		$projectMaterials = ProjectMaterial::search($request->search)
			->whereHas('project')
			->with('project')
			->get();

		return response()->json([
			'projects' => $projects,
			'projectMaterials' => $projectMaterials,
		]);
	}
}
