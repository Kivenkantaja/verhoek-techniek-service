<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate;

class RateController extends Controller
{
    public function getRate(Request $request)
    {
        $rate = Rate::where('client_id', $request->client)
                    ->where('user_id', $request->user)
                    ->where('project_id', $request->project != 0 ? $request->project : null)
                    ->first();

        return json_encode($rate);
    }

    public function saveRate(Request $request)
    {
        if($request->id) {
            $rate = Rate::where('id', $request->id)->first();
        } else {
            $rate = new Rate;
        }

        $data = $request->input();

        if(strpos($data['rate'], ','))
        {
            $data['rate'] = explode(',', $data['rate']);
            $data['rate'] = implode('.', $data['rate']);
        }

        $rate->fill($data);
        $rate->save();
    }

    public function delete(Rate $rate) {
    	$rate->delete();

    	return json_encode([
    		'status' => 0,
    		'message' => 'Tarief verwijderd.'
    	]);
    }
}
