<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Rate;

class ClientController extends Controller
{
    public function index()
    {
    	return view('clients.index');
    }

    public function view($id)
    {
        $client = Client::where('id', $id)->with(['projects'])->first();

        return view('clients.view', compact('client'));
    }

    public function make()
    {
    	return view('clients.make');
    }

    public function edit($id)
    {
        $client = Client::where('id', $id)->first();

        return view('clients.edit', compact('client'));
    }

    public function save(Request $request)
    {
        if($request->id) {
            $client = Client::where('id', $request->id)->first();
        } else {
            $client = new Client;
        }

    	$client->fill($request->input());
    	$client->save();

    	return redirect('/klanten');
    }

    public function delete($id)
    {
        $client = Client::where('id', $id)->first();
        $client->delete();

        return redirect('klanten');
    }

    public function getClient($id)
    {
        $client = Client::where('id', $id)->first();

        return json_encode($client);
    }

    public function getClients()
    {
        $clients = Client::orderBy('name')->get();

        return json_encode($clients);
    }
}
