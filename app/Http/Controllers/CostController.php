<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class CostController extends Controller
{
    public function index()
    {
    	return view('costs.index');
    }

    public function projects() 
    {
    	return json_encode(Project::where('status', 0)->with(['client' => function($query) {
    		$query->with(['rates']);
    	}, 'rates', 'user', 'hours' => function($query) {
    		$query->with('user');
    		$query->where('status', 0);
    	}, 'projectMaterials' => function($query) {
    		$query->where('status', 0);
    		$query->with('materials');
    	}])->get());
    }
}
