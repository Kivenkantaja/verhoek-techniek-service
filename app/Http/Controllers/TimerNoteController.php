<?php

namespace App\Http\Controllers;

use App\TimerNote;
use Illuminate\Http\Request;

use Auth;

class TimerNoteController extends Controller
{
    public function index()
    {
        return view('timernotes.index');
    }

    public function getTimerNotes()
    {
        if(Auth::user()->user_type == 1) {
            return json_encode(TimerNote::with('user')->get());
        } else {
            return json_encode(TimerNote::where('user_id', Auth::user()->id)->get());
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        TimerNote::create($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Notitie opgeslagen'
        ]);
    }

    public function show(TimerNote $timerNote)
    {
        //
    }

    public function edit(TimerNote $timerNote)
    {
        //
    }

    public function update(Request $request, $id)
    {
        TimerNote::where('id', $id)->first()->fill($request->input())->save();

        return json_encode([
            'status' => 1,
            'message' => 'Notitie bijgewerkt'
        ]);
    }

    public function destroy($id)
    {
        TimerNote::where('id', $id)->first()->delete();

        return json_encode([
            'status' => 1,
            'message' => 'Notitie verwijderd'
        ]);
    }
}
