<?php

namespace App\Http\Controllers;

use App\ProjectPhoto;
use Illuminate\Http\Request;
use App\ProjectMaterial;
use App\Project;
use App\Client;
use App\Hour;
use App\User;
use App\Note;
use Illuminate\Support\Facades\Auth;
use PDF;

class ProjectController extends Controller
{
    public function index()
    {
    	return view('projects.index');
    }

    public function view($id)
    {
        $project = Project::where('id', $id)->with(['client', 'user', 'photos'])->first();

        $open = Hour::where('project_id', $id)->with(['user']);
        $done = Hour::where('project_id', $id)->with(['user']);

        if(Auth::user()->user_type == 0)
        {
            $open->where('user_id', Auth::user()->id);
            $done->where('user_id', Auth::user()->id);
        }

        $hours = (object) [
            'open' => $open->where('status', 0)->get(),
            'done' => $done->where('status', 1)->get(),
        ];

        $notesCount = count(Note::where('project_id', $id)->get()->toArray());

        return view('projects.view', compact('project', 'hours', 'notesCount'));
    }

    public function edit($id)
    {
        $project = Project::where('id', $id)->first();
        $clients = Client::orderBy('name')->get();

        return view('projects.edit', compact('project', 'clients'));
    }

    public function make($id = null)
    {
    	$clients = Client::orderBy('name')->get();
    	$client = Client::where('id', $id)->first();

    	return view('projects.make', compact('clients', 'client'));
    }

    public function save(Request $request)
    {
        if($request->id) {
            $project = Project::where('id', $request->id)->first();
        } else {
            $project = new Project;
        }
    	
    	$project->fill($request->input());
    	$project->save();

    	return redirect('/projecten');
    }

    public function delete($id)
    {
        $project = Project::where('id', $id)->first();
        $project->delete();

        return redirect('/projecten');
    }

    public function getProjects(Request $request)
    {
    	$projects = (object) [
            'open' => $this->queryProjects(0, $request->search),
            'done' => $this->queryProjects(1, $request->search)
        ];

    	return json_encode($projects);
    }

    private function queryProjects($status, $search)
    {
        $data = Project::with(['client'])
                       ->rightJoin('clients', 'clients.id', '=', 'client_id')
                       ->orderBy('clients.name')
                       ->select('projects.*')
                       ->where('projects.status', $status);
        
        $terms = explode(' ', $search);

        if($search != 'null') {
            $data = $data->where(function($query) use ($terms) {
                foreach($terms as $term) {
                    $query->where('projects.name', 'like', "%$term%");
                }
            })->orWhere(function($query) use ($terms) {
                foreach($terms as $term) {
                    $query->where('clients.name', 'like', "%$term%");
                }
            })->where('projects.status', $status);
        }

        return $data->get();        
    }

    public function finish($id)
    {
        $project = Project::where('id', $id)->first();
        $project->status = 1;
        $project->save();

        return redirect('/projecten');
    }

    public function restore($id) 
    {
        $project = Project::find($id);
        $project->status = 0;
        $project->save();

        return redirect('/projecten');
    }
 
    public function invoice(Project $project, Request $request)
    {
		$status = $request->undoInvoice ? 0 : 1;
		
		Hour::whereIn('id', $request->hourIds)->update(['status' => $status]);
		ProjectMaterial::whereIn('id', $request->materialIds)->update(['status' => $status]);
    }

    public function projectMaterials(Request $request)
    {
        $open = ProjectMaterial::where('project_id', $request->id)
                               ->where('status', 0)
                               ->with(['materials'])
                               ->get();

        $done = ProjectMaterial::where('project_id', $request->id)
                               ->where('status', 1)
                               ->with(['materials'])
                               ->get();

        $dataOpen = $this->listMaterials($open);      
        $dataDone = $this->listMaterials($done); 

        usort($dataOpen, 'sortByDate');
        usort($dataDone, 'sortByDate');

        return json_encode((object) [
            'open' => array_reverse($dataOpen),
            'done' => array_reverse($dataDone)
        ]);
    }

    public function pdf(Request $request) {
        $project = Project::where('id', $request->id)->first();

        $type = $request->docType == 0 ? 'Rapportage' : 'Specificatie';
        $status = $project->status;

        $options = (object) [
            // Titel
            'description' => $request->description ? $request->description : $type.' '.$project->name,
            // Specificatie voor klant of rapport
            'docType' => (int)$request->docType,
            // Bij specificatie totalen laten zien of niet
            'totals' => $request->totals == 'false' ? false : true,
            // Uren laten zien of niet (2 = ja, 1 = alleen omschrijving, 0 = geen uren)
            'showHours' => $request->showHours,
            // Tarieven laten zien of niet; dit geld alleen voor materialen
            'showRate' => $request->showRate == 'false' ? false : true,
            // Materialen laten zien of niet
            'showMats' => $request->showMats == 'false' ? false : true,
            // BTW laten zien bij totalen
            'showVatMat' => $request->showVatMat == 'false' ? false : true,
            'showVatHours' => $request->showVatHours == 'false' ? false : true,
            // BTW gelijk toepassen
            'applyVatMat' => $request->applyVatMat == 'false' ? false : true,
            'applyVatHours' => $request->applyVatHours == 'false' ? false : true,
			// Foto's laten zien
			'showPhotos' => $request->showPhotos === 'false' ? false : true,
        ];

        $specification = $options->docType == 1 ? $this->specification($project->id, $status, $options) : null;

        $users = User::with(['hours' => function($query) use ($project, $status) {
			$query->where('show_on_report', true);
            $query->where('project_id', $project->id);
            $query->where('status', $status);
            $query->with('overtime_rate');
            $query->orderBy('date');
        }])->get();

        $users = getRate($users, $project->client_id, $project->id);
        $total_of_hours = $this->getHourTotals($users);

        $materials = $this->queryMaterials($project, $options);
        $total_price_materials = totalPrice($materials, $options->applyVatMat);
        $total_price = $total_price_materials + $total_of_hours->price;

        $compact = compact('project',
                           'users',
                           'materials',
                           'total_of_hours',
                           'total_price_materials',
                           'total_price',
                           'options',
                           'specification');

        $pdf = PDF::loadView('pdf.report', $compact);
        return $pdf->download($request->filename.'.pdf');    
    }

    private function specification($id, $status, $options)
    {
        $project = Project::where('id', $id)->with(['hours' => function($q) use ($status) {
            $q->where('status', $status);
            $q->orderBy('date');
        }, 'projectMaterials' => function($q) use ($status) {
            $q->where('status', $status);
            $q->orderBy('date');
        }, 'photos'])->first();

        $dates = array();
        foreach($project->hours as $hour)
        {
            $dates[] = $hour->date;
        }

        $dates = array_unique($dates);

        $restMaterials = $this->queryMaterials($project, $options, null, $dates);

        $data = array();
        foreach($dates as $date)
        {
            $users = User::whereHas('hours', function($q) use ($project, $date) {
                $q->where('date', $date);
                $q->where('status', $project->status);
                $q->where('project_id', $project->id);
            })->with(['hours' => function($q) use ($project, $date) {
                $q->where('date', $date);
                $q->where('status', $project->status);
                $q->where('project_id', $project->id);
                $q->orderBy('date');
            }])->get();

            $users = getRate($users, $project->client_id, $project->id);

            $materials = $this->queryMaterials($project, $options, !$date ? '1970-01-01' : $date, null);

            $data[] = (object) [
                'date' => $date,
                'users' => $users,
                'materials' => $materials,
            ];
        }

        return ((object) [
            'dates' => $data,
            'restMaterials' => $restMaterials
        ]);
    }

    private function queryMaterials($project, $options, $date = null, $dates = null)
    {
        $query = ProjectMaterial::where('project_id', $project->id)
                                ->where('status', $project->status)
                                ->with(['materials']);

        if($dates)
        {
            foreach($dates as $key => $d) {
                if($d == null) {
                    unset($dates[$key]);
                }
            }

            $query = $query->whereNotIn('date', $dates);
        }

        if($date)
        {
            $query = $query->where('date', $date);
        }

        $materials = $query->get();

        foreach($materials as $key => $material)
        {
            if($material->altPrice) {
                $rate = $material->altPrice;
            } else {
                if(isset($material->materials->price)) {
                    $rate = $material->materials->price;
                } else {
                    $rate = 0;
                }
            }

            if($options->applyVatMat && $options->docType == 1) {
                $materials[$key]->price = number_format($rate * 1.21, 2, '.', '');
                $materials[$key]->cost = number_format(($rate * $material->amount * 1.21), 2, '.', '');
            } else {
                $materials[$key]->price = number_format($rate, 2, '.', '');
                $materials[$key]->cost = number_format(($rate * $material->amount), 2, '.', '');
            }

            $materials[$key]->name = $material->altName ? $material->altName : $material->materials->name;
            $materials[$key]->number = $material->materials ? $material->materials->number : null;
        }

        return $materials;
    }

    private function listMaterials($materials)
    {
        $data = array();
        foreach($materials as $material)
        {
            if($material->altPrice) {
                $rate = $material->altPrice;
            } else {
                if(isset($material->materials->price)) {
                    $rate = $material->materials->price;
                } else {
                    $rate = 0;
                }
            }
            
            $rate = (float) $rate;
            
            $price = $material->amount * $rate;

            $data[] = (object) [
                'id' => isset($material->materials) ? $material->materials->id : 0,
                'name' => $material->altName ? 
                          $material->altName :
                          ($material->materials ? 
                           $material->materials->name : 
                           null),
                'unit' => $material->altUnit ? 
                          $material->altUnit : 
                          ($material->materials ? 
                          $material->materials->unit :
                          null),
                'rate' => number_format($rate, 2, ',', ''),
                'price' => number_format($price, 2, ',', ''),
                'number' => isset($material->materials) ? $material->materials->number : null,            
                'link_id' => $material->id,
                'amount' => $material->amount,
                'date' => $material->date ? date('d-m-Y', strtotime($material->date)) : null,
                'alt' => $material->altName ? true : false,
                'status' => $material->status,
                'description' => isset($material->materials->description) ? $material->materials->description : null,
				'updated_at' => isset($material->materials->updated_at) ? date('d-m-Y', strtotime($material->materials->updated_at)) : null,
            ];
        }        

        return $data; 
    } 

    public function getHourList(Request $request)
    {
		$project = Project::find($request->id);
		
        return json_encode([
            0 => $this->queryHourList(0, $project),
            1 => $this->queryHourList(1, $project),
        ]);
    }

    private function queryHourList($status, Project $project)
    {
        $data = $project->hours();
		
        if(Auth::user()->user_type === 0) {
            $data->where('user_id', Auth::user()->id);
        }

        $data->where('status', $status);
		$data->OrderByDesc('date');
		
		return $data->get()->load('user')
			->map(function ($hour) {
			$hour->output = outputHours($hour->minutes);
			$hour->outputDate = date('d-m-Y', strtotime($hour->date));
			
			return $hour;
		});
    }

    private function getHourTotals($users)
    {
        $total_prices = array();
        $total_hours = array();

        foreach($users as $user)
        {
            $hours = totalHours($user->hours) / 60;
            // $total_prices[] = $hours * $user->rate;

            $total_prices[] = totalPriceHours($user->hours, $user->rate, false, 2);

            $total_hours[] = $hours; 
        }

        return (object) [
            'hours' => outputHours(array_sum($total_hours) * 60),
            'price' => $this->sum($total_prices)
        ];
    }

    private function sum($array) {
        $total = 0;
        foreach($array as $item) {
            if(strpos($item, ',') !== false) {
                $n = explode(',', $item);
                $n = (float)implode('.', $n);
            } else {
                $n = (float)$item;
            }

            $total = $total + $n;
        }

        return $total;
    }

    public function lastUsedProject()
    {
        $hour = Hour::orderBy('created_at', 'desc')
                    ->where('user_id', Auth::user()->id)
                    ->first();

        if(isset($hour->id)) {
            return $hour->project_id;
        }
    }
}
