<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(user()->user_type === 0) {
            return redirect('/uren');
        }

        return $next($request);
    }
}
