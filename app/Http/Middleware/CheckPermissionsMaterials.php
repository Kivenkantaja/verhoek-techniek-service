<?php

namespace App\Http\Middleware;

use Closure;

class CheckPermissionsMaterials
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
		if(!user()->user_type && (!user()->permissions || !user()->permissions->seeMaterials)) {
			return redirect('/uren');
		}
		
		return $next($request);
    }
}
