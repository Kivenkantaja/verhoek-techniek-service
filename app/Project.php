<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'name', 
    	'user_id', 
    	'client_id',
        'rate',
        'costs_overview'
    ];

    public function client()
    {
    	return $this->belongsTo('App\Client');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function hours()
    {
    	return $this->hasMany('App\Hour');
    }
	
	public function openHours()
	{
		return $this->hasMany(Hour::class)->where('status', 0);
	}

    public function rates()
    {
        return $this->hasMany('App\Rate');
    }

    public function projectMaterials()
    {
        return $this->hasMany('App\ProjectMaterial');
    } 
    
    public function photos() {
    	return $this->hasMany(ProjectPhoto::class);
	}
}
