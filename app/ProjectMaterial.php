<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMaterial extends Model
{
    protected $table = 'project_materials';

    protected $fillable = [
    	'project_id',
    	'material_id',
    	'amount',
        'date',
        'altName', 
        'altPrice',
        'altUnit',
		'status',
    ]; 

    public function materials()
    {
    	return $this->belongsTo('App\Material', 'material_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

	public function scopeSearch($query, $search)
	{
		return $query
			->rightJoin('materials', 'project_materials.material_id', '=', 'materials.id')
			->where('project_materials.altName', 'like', '%' . $search . '%')
			->orWhere('materials.name', 'like', '%' . $search . '%');
	}
}
