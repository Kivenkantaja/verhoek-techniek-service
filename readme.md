# Verhoek Techniek Service

A [Verhoek Development](https://verhoek.dev) project.


## Deployment

| Environment | URL                              | Registrar | Hosting   |
|:------------|:---------------------------------|:----------|:----------|
| Staging     | —                                | —         | —         |
| Production  | http://verhoektechniekservice.nl | [Argeweb] | [Argeweb] |

[Argeweb]: https://www.argeweb.nl/inloggen/
