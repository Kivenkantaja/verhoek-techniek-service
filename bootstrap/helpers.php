<?php

use Illuminate\Support\Facades\Auth;

function user()
{
	return Auth::user();
}

function dateRange($start, $end)
{
    $begin = new DateTime($start);
    $end = new DateTime($end);
    $end = $end->modify( '+1 day' );

    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);

    $output = array();
    foreach($daterange as $date)
    {
        $output[] = $date->format('Y-m-d');
    }

    return $output;    
}

function weekByNumber($week, $year)
{
    $output = (object) [];
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $first = $dto->format('Y-m-d');
    $dto->modify('+6 days');
    $last = $dto->format('Y-m-d');
    $previous = $year - 1;

    /* Checking if this week transitions between a month or a year
    *  0 = all in the same month
    *  1 = transition between months
    *  2 = transition between years */
    if(date('m', strtotime($first)) == date('m', strtotime($last))) {
        $type = 0;
    } else {
        if(date('Y', strtotime($first)) == date('Y', strtotime($last))) {
            $type = 1;
        } else {
            $type = 2;
        }
    }

    $output = (object) [
        'first' => (object) [
            'date' => $first,
            'month' => date('m', strtotime($first)),
            'monthName' => monthList()[date('m', strtotime($first))],
            'year' => date('Y', strtotime($first)),
        ],
        'last' => (object) [
            'date' => $last,
            'month' => date('m', strtotime($last)),
            'monthName' => monthList()[date('m', strtotime($last))],
            'year' => date('Y', strtotime($last))            
        ],
        'week' => $week,
        'year' => $year,
        'lastWeek' => date('W', strtotime("28-12-$year")),
        'lastYear' => date('W', strtotime("28-12-$previous")),
        // applying the transition check
        'type' => $type
    ];

    return $output;
}

function formatHours($data)
{
	$minutes = array();
	foreach($data as $hour) {
		$minutes[] = $hour->minutes;
	}

	$total = array_sum($minutes);
	$hours = (int) ($total / 60);
	$minutes = sprintf('%02d', $total % 60);
    $output = "$hours:$minutes";

    if($hours == 0 && $minutes == 0) {
        $output = null;
    }

	return $output;
}

function weekDays()
{
    return [
        1 => 'ma',
        2 => 'di',
        3 => 'wo',
        4 => 'do',
        5 => 'vr',
        6 => 'za',
        7 => 'zo'
    ];
}

function monthList()
{
    return [
        '01' => 'januari',
        '02' => 'februari',
        '03' => 'maart',
        '04' => 'april',
        '05' => 'mei',
        '06' => 'juni',
        '07' => 'juli',
        '08' => 'augustus',
        '09' => 'september',
        '10' => 'oktober',
        '11' => 'november',
        '12' => 'december'
    ];
}

function outputHours($min) 
{
    $hours = (int)($min / 60);
    $minutes = sprintf('%02d', $min % 60);    

    return "$hours:$minutes";
}

function sortByDate($a, $b) {
    return strtotime($a->date) - strtotime($b->date);
}

function sortByString($a, $b)
{
    return strcmp($a->name, $b->name);
}

function totalHours($hours)
{
    $array = array();
    foreach($hours as $hour)
    {
        $array[] = $hour->minutes;
    }

    return array_sum($array);
}

function totalPrice($materials, $applied = false)
{
    $prices = array();

    foreach($materials as $key => $material)
    {
        $price = number_format(($material->amount * $material->price), 2, '.', '');

        $prices[] = $price;
    }

    $price = array_sum($prices);

    if($applied) {
        $price = $price / 1.21;
    } 

    return number_format($price, 2, '.', '');
}

function dutchDate($date)
{
    if(!$date || $date == 'Invalid date')
    {
        return null;
    }
    
    // if(date('Y', strtotime($date)) == '1970') 
    // {
    //     dd($date);
    // }

    return weekDays()[date('N', strtotime($date))].' '.
    date('d', strtotime($date)).' '.
    monthList()[date('m', strtotime($date))].' '.
    date('Y', strtotime($date));
}

function getRate($users, $client, $project, $view = false)
{
    foreach($users as $key => $user) {
        $rate = 0;

        if($project) {
            $rate = App\Rate::where('project_id', $project)->where('user_id', $user->id)->first();
        }

        if(!$view) {
            if(!$rate || $rate->rate == 0) {
                $rate = App\Rate::where('client_id', $client)->where('user_id', $user->id)->first();
            }
        }    
        
        $users[$key]->rate_id = $rate ? $rate->id : 0;
        $users[$key]->rate = $rate ? $rate->rate : 0;
    } 

    return $users;  
}

function totalPriceHours($hours, $rate, $applyVat, $overtime = 0)
{
    if($applyVat)
    {
        $rate = $rate * 1.21;
    }

    $array = [];
    foreach($hours as $hour)
    {
        if($hour->overtime_rate_id) {
            if($overtime > 0) {
                $percentage = 1 + ($hour->overtime_percentage / 100);
                $array[] = ($hour->minutes / 60) * ($rate * $percentage);
            }
        } else {
            if($overtime == 0 || $overtime == 2) {
                $array[] = ($hour->minutes / 60) * $rate;
            }
        }
    }

    return number_format(array_sum($array), 2, ',', '');
}

function outputPriceHours($rate, $minutes, $applyVat)
{
    if($applyVat) {
        $rate = $rate * 1.21;
    }

    return number_format($rate * ($minutes / 60), 2, ',', '');
}

// When hours or materials have VAT turned off
function totalPriceException($total_materials, $total_hours, $options) {
    $total_materials = $options->applyVatMat ? $total_materials * 1.21 : $total_materials;
    $total_hours = $options->applyVatHours ? $total_hours * 1.21 : $total_hours;

    return number_format($total_materials + $total_hours, 2, ',', '.');
}

function includesHours($hours, $overtime) {
    $count = 0;
    foreach($hours as $hour) {
        if($overtime) {
            if($hour->overtime_rate_id) {
                $count++;
            }
        } else {
            if(!$hour->overtime_rate_id) {
                $count++;
            }
        }
    }

    return $count > 0 ? true : false;
}
