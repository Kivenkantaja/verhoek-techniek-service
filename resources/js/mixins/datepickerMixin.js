import Datepicker from 'vuejs-datepicker';
import {nl} from 'vuejs-datepicker/dist/locale';
import moment from "moment";	

export const datepickerMixin = {
	methods: {
        dateFormat(date) {
          return moment(date).format('DD-MM-YYYY');
        }, 		
		
		formatDate(date) {
		    let array = [
		        this.leadingZero(date.getDate()),
		        this.leadingZero(date.getMonth() + 1),
		        date.getFullYear(),
		    ];

		    return array.join('-');
		},
		
		leadingZero(number) {
		    if(number < 10)
		    {
		        return '0' + number.toString();
		    } else 
		    {
		        return number;
		    }
		}
	},
	
	components: {
		Datepicker
	},
	
	data() {
		return {
			nl: nl
		}
	}
}
