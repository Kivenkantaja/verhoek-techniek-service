export const permissionsMixin = {
	watch: {
        user(val) {
            if(val)
            {
                let date = new Date();
                let limit = new Date();
                limit.setHours(12, 0, 0);

                if(val.user_type == 0) {
                    if(date < limit) {
                        this.minDate = date.setDate(date.getDate() - 2);
                        this.deadline = false;
                    } else {
                        this.minDate = date.setDate(date.getDate() - 1);
                        this.deadline = true;
                    }                       
                }
            }
        },		
	}
}