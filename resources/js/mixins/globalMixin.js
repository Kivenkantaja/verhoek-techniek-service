export const globalMixin = {
	data() {
		return {
			errors_list: []
		}
	},
	methods: {
		checkString(input) {
        	return input && isNaN(input);
        },
		
		save(url, fields) {
			if(!fields.id) {
				axios.post(url, fields).then(response => {
					this.handleSaved(response.data);
				}).catch(error => {
					this.catch(error);
				});
			} else {
				axios.put(url+'/'+fields.id, fields).then(response => {
					this.handleSaved(response.data);
				}).catch(error => {
					this.catch(error);
				});
			}
		},
		
		destroy(url, id) {
			axios.delete(url+'/'+id).then(response => {
				this.handleSaved(response.data);
			}).catch(error => {
				this.catch(error);
			});
		},
		
		catch(event) {
			this.errors_list.push(event);
		},
		
		clearErrors() {
			this.errors_list = [];
		}
	}
}
