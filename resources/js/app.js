require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');
window._ = require('lodash');
window.numeral = require('numeral');
window.toastr = require('toastr');

import Datepicker from 'vuejs-datepicker';
import {nl} from 'vuejs-datepicker/dist/locale';
import TextareaAutosize from 'vue-textarea-autosize';
import {globalMixin} from './mixins/globalMixin';
import GlobalSearch from './components/parts/global-search.vue';
 
Vue.use(TextareaAutosize)

Vue.component('form-input', require('./components/forms/form-input.vue').default);
Vue.component('empty-input', require('./components/forms/empty-input.vue').default);
Vue.component('project-materials', require('./components/lists/project-materials.vue').default);
Vue.component('modal', require('./components/modal.vue').default);
Vue.component('errors', require('./components/parts/errors.vue').default);
Vue.component('pagination', require('./components/parts/pagination').default);
Vue.component('vue-table', require('./components/parts/vue-table').default);

Vue.mixin(globalMixin);

if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js')
		 .then(() => console.log('Service worker works'))
		 .catch(() => console.log('Service worker denied'));
}

const app = new Vue({
	mounted() {
		axios.get('/getClients').then(response => this.clients = response.data);
        axios.get('/getProjects').then(response => this.projects = response.data);
        axios.get('/getMaterials').then(response => this.materials = response.data);
        axios.get('/getUsers').then(response => this.users = response.data);

        axios.get('/getUser').then(response => {
            this.user = response.data;         
        });       
	},
	
    el: '#app',
	
    data: {
        showNav: false,
    	data: {},
    	clients: null,
        projects: null,
        materials: null,
        users: null,
        user: null,
        material_form: null,
        hours_form: null,
        formData: null,
    	showModal: false,
        message: null,
        confirmDelete: false,
        nl: nl,
        view: 1,
        page: 0,
        mode: null,
        minDate: null,
        deadline: false,
        material_search: null,
        project_search: null,
        projectMaterials: {},
        confirm_status: null,
        displayHours: {},
        showProfile: false,
    },
	
    methods: {
        refreshProjectMaterials() {
            this.$refs['projectMaterialsOpen'].refresh();
            this.$refs['projectMaterialsDone'].refresh();
        },
		
        currentMonth()
        {
            axios.get('/getHours').then(response => this.hours = response.data);
        },
		
        dateFormat(date) {
          return moment(date).format('DD-MM-YYYY');
        }, 
		
    	loadClient(id) {
    		axios.get('getClient/'+id).then(response => this.data = {client: response.data});
    	},
		
        deleteHour(id) 
        {
            axios.get('/uren/verwijderen/'+id).then(response => {
                this.showModal = false;
                this.message = response.data;
                this.$refs.hours.refresh();
            });
        },
		
        inArray(val, array) 
        {
            let output = false;
            array.map(function(e) {
                if(e == val) {
                    output = true;
                }
            })
            return output;
        },
		
        closeModal() {
            this.showModal = false; 
            this.data = {}; 
            this.confirmDelete = false; 
            this.displayHours = {};
            this.page = 0;
            this.project_search = null;
        },
		
        handleSavedMaterials(event) {
            this.message = event;
            this.material_form = null;

            this.$refs.projectMaterialsOpen.refresh();
            this.$refs.projectMaterialsDone.refresh();
        },
		
        handleSavedHours(event) {
            this.hours_form = null;
            this.message = event;

            if (this.$refs.hoursList0) {
                this.$refs.hoursList0.loadData();                
                this.$refs.hoursList1.loadData();
            }
			
			if (this.$refs.projectMaterialsOpen) {
				this.$refs.projectMaterialsOpen.refresh();
				this.$refs.projectMaterialsDone.refresh();
			}

            if(this.$refs.hours) {
                this.$refs.hours.loadHours();            
            }
        },
		
        handleError(event) {
            this.message = event;

            this.hours_form = null;
            this.material_form = null;
        },
		
        previous() {
            window.history.back();
        }
    },
    watch: {
        message(val) {
            if(val != null) {
                window.setTimeout(function() {
                    app.message = null;
                }, 2000);
            }
        },
        material_search(val) {
            axios.get('/getMaterials?search='+val).then(response => {
                this.materials = response.data
                if(response.data[0]) {
                    this.material_form.id = response.data[0].id;
                }
            });
        },
    },
    components: {
        Datepicker: Datepicker,
		GlobalSearch: GlobalSearch,
        hours: require('./components/lists/hours.vue').default,
        projectList: require('./components/lists/project-list.vue').default,
        users: require('./components/lists/users.vue').default,
        rates: require('./components/lists/rates.vue').default,
        hoursList: require('./components/lists/hours-list.vue').default,
        invoiceForm: require('./components/forms/invoice-form.vue').default,
        pdfForm: require('./components/forms/pdf-form.vue').default,
        materials: require('./components/lists/materials.vue').default,
        feedback: require('./components/lists/feedback.vue').default,
        activities: require('./components/lists/activities.vue').default,
        notes: require('./components/lists/notes.vue').default,
        hourForm: require('./components/forms/hour-form.vue').default,
        projectMaterialForm: require('./components/forms/project-material-form.vue').default,
        navItem: require('./components/nav-item.vue').default,
        profile: require('./components/profile.vue').default,
        costs: require('./components/lists/costs.vue').default,
        timerNotes: require('./components/lists/timer-notes.vue').default,
        minStock: require('./components/materials/min-stock.vue').default,
        uploadPhoto: require('./components/materials/upload-photo.vue').default,
		clients: require('./components/lists/clients.vue').default,
		clientForm: require('./components/forms/client-form.vue').default,
		projectPhotos: require('./components/projects/ProjectPhotos').default,
		projectShow: require('./components/projects/ProjectShow').default,
    }
});

function today()
{
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); 
    let yyyy = today.getFullYear();

    today = dd + '-' + mm + '-' + yyyy;

    return today;
}

function formatDate(date)
{
    let array = [
        leadingZero(date.getDate()),
        leadingZero(date.getMonth() + 1),
        date.getFullYear(),
    ];

    return array.join('-');
}

function leadingZero(number)
{
    if(number < 10)
    {
        return '0' + number.toString();
    } else 
    {
        return number;
    }
}

