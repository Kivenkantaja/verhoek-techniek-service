@extends('layout')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="pull-left">
		    	<h1>Dashboard</h1>
		    </div>
		    <div class="pull-right">
		    	<button onclick="window.location.reload(true);" class="update-button btn btn-primary">
		    		Update
		    	</button>
		    </div>			
		</div>
	</div>
	
	<global-search></global-search>

	<min-stock @message="message = $event"></min-stock>


    <!-- @if(count($data) > 0) -->
	    <!-- <h4 class="align-with-table">Niet gefactureerde projecten</h4> -->
	    <!-- <table class="table data-table"> -->
	    	<!-- <thead> -->
	    		<!-- <th> Project </th> -->
	    		<!-- <th> Klant </th> -->
	    		<!-- <th style="min-width: 100px"> Vanaf </th> -->
	    	<!-- </thead> -->
	    	<!-- <tbody> -->
	    		<!-- @foreach($data as $row) -->
					<!-- <tr> -->
						<!-- <td> <a href="/projecten/inzien/{{ $row->project->id }}">{{ $row->project->name }}</a> </td> -->
						<!-- <td> --> 
							<!-- @if($row->project->client) -->
								<!-- <a href="/klanten/inzien/{{ $row->project->client->id }}"> --> 
								<!-- {{ $row->project->client->name }} --> 
							<!-- @else -->
								<!-- Verwijderde klant -->
							<!-- @endif -->
						<!-- </a> </td> -->
						<!-- <td> {{ date('d-m-Y', strtotime($row->date)) }} </td> -->
					<!-- </tr> -->
	    		<!-- @endforeach -->
	    	<!-- </tbody> -->
	    <!-- </table> -->
	<!-- @endif -->
@endsection
