@extends('layout')

@section('content')
	<hours :user="{{ Auth::user() }}" ref="hours" 
		   v-on:form="hours_form = $event"
		   v-on:saved="handleSavedHours($event)"></hours>
@stop
