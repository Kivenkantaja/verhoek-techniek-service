<h1>Uren</h1>
<h4 v-if="hours">  
	<span v-if="hours.dateData.type == 0"> 
		@{{ hours.dateData.first.monthName }} @{{ hours.dateData.first.year }} 
	</span>
	<span v-if="hours.dateData.type == 1"> 
		@{{ hours.dateData.first.monthName }}/@{{ hours.dateData.last.monthName }} @{{ hours.dateData.first.year }} 
	</span>
	<span v-if="hours.dateData.type == 2"> 
		@{{ hours.dateData.first.monthName }} @{{ hours.dateData.first.year }} - 
		@{{ hours.dateData.last.monthName }} @{{ hours.dateData.last.year }}
	</span>
</h4>

<div style="margin-bottom: 10px">
	<button style="margin-right: 2px" class="btn btn-default" v-on:click="currentMonth()"> 
		week @{{ hours ? hours.dateData.week : null }} 
	</button>

	<button class="btn btn-primary" v-on:click="previousMonth()"> < </button> 
	<button class="btn btn-primary" v-on:click="nextMonth()"> > </button>

	<span style="margin-left: 10px; position: relative">
		<button class="btn btn-default" v-on:click="filters.show = !filters.show"> Filters </button>
		<div v-if="filters.show" class="filters">
			<button v-for="user in users.active" 
					class="btn btn-primary btn-block" 
					style='margin-top: 0px'
					v-on:click="addToFilter(user.id)">
				@{{ user.name }} <span v-if="inArray(user.id, filters.users)"> <i class="fa fa-check"></i> </span>
			</button>
			<button style='margin-top: 0px' class="btn btn-primary btn-block" v-on:click="applyFilters"> 
				Filters toepassen 
			</button>
		</div>
	</span>
</div>

<div v-if="hours" class="week">
	<div class="project"> Dag <br> <span class="hidden-text">.</span>  </div>

	<div class="weekDays">
		<div v-for="day in hours.headRow"> 
			<div v-on:click="hourForm(null, day.date, null)" class="day">
				@{{ day.day }} <br> <b> @{{ day.weekDay }} </b>
			</div>
		</div>
	</div>

	<div v-for="row in hours.hourData">
		<div v-bind:title="row.project" class="project">
			@{{ row.client }} - @{{ row.project }}
		</div>

		<div class="weekDays">
			<div v-for="day in row.period" v-on:click="hourForm(day.hours, day.date, row.project_id)" class="day">
				<span class="hidden-text" v-if="!day.hours">.</span>
				<span v-else> @{{ day.hours }} </span>
			</div>
		</div>
	</div>

	<div class="project"> <b>Totalen (@{{ hours.total }})</b> </div>
	<div class="weekDays">
		<div v-for="day in hours.footRow"> 
			<div class="day">
				@{{ day.output }}
			</div>
		</div>		
	</div>	
</div>