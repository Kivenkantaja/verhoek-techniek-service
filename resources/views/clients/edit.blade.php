@extends('layout')

@section('content')
	<h1> <a href="/klanten">Klanten</a>/bewerken/{{ $client->name }} </h1>
	<button type="button" v-on:click="previous()" class="btn btn-primary">Terug <i class="fa fa-arrow-left"></i> </button>

	<form class="tab" action="/klanten/opslaan" method="POST">
		{{ csrf_field() }}
		<div class="tab-title">
			<h3>Klant bewerken</h3>
		</div>

		<input type="hidden" value="{{ $client->id }}" name="id">

		<div class="tab-content">
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-user"></i>
				</div>
				<div class="form-input">
					<input value="{{ $client->name }}" type="text" class="form-control" name="name" placeholder="Naam*" required>
				</div>				
			</div>
			
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-map-marker"></i>
				</div>
				<div class="form-input form-input-1">
					<input value="{{ $client->postal_code }}" type="text" class="form-control" name="postal_code" placeholder="Postcode*" required>
				</div>
				<div class="form-input form-input-2">
					<input value="{{ $client->adress }}" type="text" class="form-control" name="adress" placeholder="Adres*" required>
				</div>				
			</div>		

			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-building"></i>
				</div>
				<div class="form-input">
					<input value="{{ $client->town }}" type="text" class="form-control" name="town" placeholder="Woonplaats*">
				</div>				
			</div>

			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-envelope"></i>
				</div>
				<div class="form-input">
					<input value="{{ $client->email }}" type="text" class="form-control" name="email" placeholder="E-mailadres">
				</div>				
			</div>

			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-phone"></i>
				</div>
				<div class="form-input">
					<input value="{{ $client->phone }}" type="text" class="form-control" name="phone" placeholder="Telefoonnummer">
				</div>				
			</div>

			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-sticky-note"></i>
				</div>
				<div class="form-input">
					<input value="{{ $client->kvk }}" type="text" class="form-control" name="kvk" placeholder="KVK nummer">
				</div>				
			</div>		

			<div class="form-item no-margin">
				<div class="form-icon"></div>
				<div class="form-input form-input-1">
					<a href="/klanten" class="btn btn-default btn-block"> Annuleren <i class="fa fa-arrow-left"></i> </a>
				</div>

				<div class="form-input form-input-2">
					<button class="btn btn-primary btn-block"> Opslaan <i class="fa fa-save"></i> </button>
				</div>
			</div>		
		</div>
	
		
	</form>
@stop