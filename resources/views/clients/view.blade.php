@extends('layout')

@section('content')
	<h1>{{ $client->name }}</h1>

	<button type="button" v-on:click="previous()" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> </button>
	<a href="/klanten/bewerken/{{ $client->id }}" class="btn btn-default"> <i class="fa fa-pencil-alt"> </i> </a>

	<button type="button" v-on:click="confirmDelete = true; showModal = true;" class="btn btn-primary"> 
		<i class="fa fa-trash"></i> 
	</button>

	<table class="table table data-table info-table">
		<thead>
			<th colspan="2">Gegevens</th>
		</thead>
		<tbody>
			<tr>
				<td> <b>Postcode + adres</b>  </td>
				<td> {{ $client->postal_code }} {{ $client->adress }} </td>
			</tr>
			@if($client->town)
				<tr>
					<td> <b>Woonplaats</b> </td>
					<td> {{ $client->town }} </td>
				</tr>
			@endif
			@if($client->email)
				<tr>
					<td> <b>E-mailadres</b> </td>
					<td> {{ $client->email }} </td>
				</tr>
			@endif
			@if($client->phone)
				<tr>
					<td> <b>Telefoonnummer</b> </td>
					<td> {{ $client->phone }} </td>
				</tr>
			@endif
			@if($client->kvk)
				<tr>
					<td> <b>KVK</b> </td>
					<td> {{ $client->kvk }} </td>
				</tr>
			@endif
			@if($client->rate)
				<tr>
					<td> <b>Tarief</b> </td>
					<td> €{{ $client->rate }} </td>
				</tr>
			@endif
		</tbody>
	</table>	
	
	@if(isset($client->projects[0]))
		<table class="table table data-table">
			<thead>
				<th>Projecten</th>
			</thead>
			<tbody>
				@foreach($client->projects as $clientProject)
					<tr>
						<td> <a href="/projecten/inzien/{{ $clientProject->id }}">{{ $clientProject->name }}</a> </td>
					</tr>
				@endforeach
			</tbody>	
		</table>
	@endif

	<rates status="active" client="{{ $client->id }}"></rates>
	<rates status="disabled" client="{{ $client->id }}"></rates>
@stop
