@extends('layout')

@section('content')
	<h1> <a href="/klanten">Klanten</a>/nieuw </h1>
	<button type="button" v-on:click="previous()" class="btn btn-primary">Terug <i class="fa fa-arrow-left"></i> </button>

	<client-form>
		{{ csrf_field() }}
	</client-form>
@stop
