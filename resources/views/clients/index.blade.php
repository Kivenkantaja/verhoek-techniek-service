@extends('layout')

@section('content')
	<h1>Klanten</h1>
	<a href="/klanten/nieuw" class="btn btn-primary">Nieuwe klant <i style="margin-left: 5px;" class="fa fa-plus"></i> </a>
	
	<clients></clients>
@stop
