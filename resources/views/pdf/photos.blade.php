@if ($options->showPhotos)
	<div class="page-break">
		<h1>Foto's</h1>
		
		@foreach($project->photos as $photo)
			<div class="pdf-photo-wrapper">
				<img src="{{ storage_path('app/public/'.$photo->path) }}" class="pdf-photo">
				
				<div class="pdf-photo-description">
					{{ $photo->description }}
				</div>
			</div>
		@endforeach
	</div>
@endif
