@if(includesHours($user->hours, true))	
	<h3>Meerwerk</h3>
	<table class="table">
		<tr>
			<th style="width: 70px">Datum</th>
			@if($options->showHours > 0)
				<th style="width: 30px">Uren</th>
				@if($options->showHours > 1)
					<th style="width: 120px">Meerwerk</th>
					<th style="width: 60px">Prijs</th>
				@endif
			@endif
			<th>Omschrijving</th>
		</tr>
		@foreach($user->hours as $hour)
			@if($options->showHours > -1 && $hour->overtime_rate_id)
				<tr>
					<td> {{ $hour->date ? date('d-m-Y', strtotime($hour->date)) : 'Geen datum' }} </td>
					@if($options->showHours > 0)
						<td> {{ outputHours($hour->minutes) }} </td>
						@if($options->showHours > 1)
							<td> {{ $hour->overtime_rate->name }} ({{ $hour->overtime_percentage }}%) </td>
							<td> 
								<span style="font-family: tahoma !important"> &#0128;</span><!--
								-->{{ number_format(($user->rate * ($hour->minutes / 60)) * (1 + ($hour->overtime_percentage / 100)), 2, ',', '') }} 
							</td>
						@endif
					@endif
					<td> {{ $hour->description ? 
							$hour->description : 
							'Geen opmerking ingevoerd' }} 
					</td>
				</tr>
			@endif
		@endforeach
		@if($options->showHours > 0)
			<tr>
				<td> <b> Totaal: </b> </td>
				<td> <b> {{ outputHours(totalHours($user->hours)) }} </b> </td>
				@if($options->showHours > 1)
					<td> <b> €{{ totalPriceHours($user->hours, $user->rate, false, 1) }} </b> </td>
				@endif
			</tr>
		@endif
	</table>
@endif