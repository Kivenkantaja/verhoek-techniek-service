<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta charset="UTF-8">
	
	<style>
		@page {
			margin: 0cm 0cm;
		}

		@font-face {
			font-family: 'roboto';
			src: url({{ storage_path('fonts/Roboto-Regular.ttf') }}) format("truetype");
			font-weight: 400; 
			font-style: normal; /
		}

		@font-face {
			font-family: 'roboto-bold';
			src: url({{ storage_path('fonts/Roboto-Bold.ttf') }}) format("truetype");
			font-weight: 700; 
			font-style: normal; 
		}
	</style>

	<link rel="stylesheet" href="{{ storage_path().'/pdf/style.css' }}">
</head>
<body>
	<header>
		<img src="{{ storage_path().'/pdf/logo.png' }}">
		<div class="white-bar"></div>
		<div class="blue-bar"></div>
	</header>

	<footer>
		Wij zijn o.a. werkzaam in Zuid-Holland, Utrecht, Noord-Brabant en Midden-Limburg <br>
		info@verhoektechniekservice.nl - www.verhoektechniekservice.nl <br>
		KVK: 63147521 - Rabobank: NL70 RABO 0302 951 717
		<div class="white-bar"></div>
		<div class="blue-bar"></div>
	</footer>
	
	<main>
		<div class="content">
			<h1>{{ $options->description }}</h1>
			@if($options->docType == 0)
				@foreach($users as $user)
					@if(count($user->hours) > 0)
						<h2>{{ $options->showHours > 0 ? 'Uren' : 'Rapport' }} {{ $user->name }} </h2>
						@if($user->rate > 0 && $options->showHours > 1)
							<h3>
								Tarief: <span style="font-family: tahoma !important"> &#0128;</span><!--
								-->{{ number_format($user->rate, 2, ',', '') }}
							</h3>
						@endif
						@include('pdf.personal-report.table-hours')
						@include('pdf.personal-report.table-hours-overtime')
					@endif
				@endforeach
					
				@if(count($materials) > 0 && $options->showMats)
					<h2>Materialen</h2>
					<table class="table">
						<tr>
							<th>Datum</th>
							<th>Aantal</th>
							<th>Materiaal</th>
							<th>Artikelnummer</th>
							@if($options->showRate)
								<th>Prijs p/e</th>
								<th>Prijs totaal</th>
							@endif
						</tr>
						@foreach($materials as $material)
							<tr>
								<td> {{ date('d-m-Y', strtotime($material->date)) }} </td>
								<td> {{ $material->amount }} {{ $material->unit }} </td>
								<td> {{ $material->name }} </td>
								<td> {{ $material->number ? $material->number : null }} </td> 
								@if($options->showRate)
									<td> 
										<span style="font-family: tahoma !important"> &#0128; </span>  
										{{ number_format($material->price, 2, ',', '') }}
									</td>
									<td> 
										<span style="font-family: tahoma !important"> &#0128; </span> 
										{{ number_format($material->cost, 2, ',', '') }}
									</td>
								@endif
							</tr>
						@endforeach
						<tr>
							<td colspan="4"></td>
							<td> <b> Totaal: </b> </td>
							<td> <b> <span style="font-family: tahoma !important"> &#0128; </span><!-- 
							-->{{ number_format($total_price_materials, 2, ',', '') }} </b> </td>
						</tr>
					</table>
				@endif
				
				<h2>Totalen</h2>
				<div style="width: 50%">
					<table class="table data-table">
						@if($options->showMats)
							<tr>
								<td> Totale prijs materialen: </td>
								<td> 
									<span style="font-family: tahoma !important"> &#0128; </span><!--
									-->{{ number_format($total_price_materials, 2, ',', '') }} 
								</td>
							</tr>
						@endif
						@if($options->showHours > 0)
							@if($options->showHours > 1)
								<tr>
									<td> Totale prijs uren: </td>
									<td> 
										<span style="font-family: tahoma !important"> &#0128; </span><!--
										-->{{ number_format($total_of_hours->price, 2, ',', '') }} 
									</td>
								</tr>
								@if($options->showMats)
									<tr>
										<td> Totale prijs: </td>
										<td> 
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ number_format($total_price, 2, ',', '') }} 
										</td>
									</tr>
								@endif
							@endif
							<tr>
								<td> Totaal aantal uren: </td>
								<td> {{ $total_of_hours->hours }} </td> 
							</tr>
						@endif
					</table>
				</div>
			@endif

			@if($options->docType == 1)
				@foreach($specification->dates as $day)
					
					@if($options->showHours > 1)
						<h2> {{ dutchDate($day->date) ? dutchDate($day->date) : 'Overige uren' }} </h2>
					@else
						<h2> <h2> {{ dutchDate($day->date) ? dutchDate($day->date) : 'Overig' }} </h2> </h2>
					@endif
					
					@foreach($day->users as $user)
						@if(count($user->hours) > 0)
							<h3>{{ $options->showHours < 1 ? 'Rapport' : 'Uren' }} {{ $user->name }} </h3>
							@if($user->rate > 0 && $options->showHours > 1)
								<h4>
									Tarief: 
									@if($options->applyVatHours)
										<span style="font-family: tahoma !important"> &#0128;</span><!--
										-->{{ number_format($user->rate * 1.21, 2, ',', '') }}
									@else
										<span style="font-family: tahoma !important"> &#0128;</span><!--
										-->{{ number_format($user->rate, 2, ',', '') }}
									@endif
								</h4>				
							@endif	
							@include('pdf.client-report.table-hours')
							@include('pdf.client-report.table-hours-overtime')			
						@endif
					@endforeach	
					
					@if(count($day->materials) > 0 && $options->showMats)
						<h3>Materialen</h3>
						<table class="table">
							<tr>
								<th>Aantal</th>
								<th>Materiaal</th>
								@if($options->showRate)
									<th>Prijs p/e</th>
									<th>Prijs totaal</th>
								@endif
							</tr>
							@foreach($day->materials as $material)
								<tr>
									<td> {{ $material->amount }} {{ $material->unit }} </td>
									<td> {{ $material->name }} </td>
									@if($options->showRate)
										<td> 
											<span style="font-family: tahoma !important"> &#0128; </span>  
											{{ number_format($material->price, 2, ',', '') }}
										</td>
										<td> 
											<span style="font-family: tahoma !important"> &#0128; </span> 
											{{ number_format($material->cost, 2, ',', '') }}
										</td>
									@endif
								</tr>
							@endforeach

							@if($options->showRate)
								<tr>
									<td></td> <td></td>
									<td> <b> Totaal: </b> </td>
									<td> <b> 
										<span style="font-family: tahoma !important"> &#0128; </span><!--
										-->{{ number_format(totalPrice($day->materials, false), 2, ',', '') }} 
										</b> 
									</td>
								</tr>
							@endif
						</table>		
					@endif			

				@endforeach

				@if(count($specification->restMaterials) > 0 && $options->showMats)
					<h3>Resterende materialen</h3>
					<table class="table">
						<tr>
							<th>Datum</th>
							<th>Aantal</th>
							<th>Materiaal</th>
							@if($options->showRate)
								<th>Prijs p/e</th>
								<th>Prijs totaal</th>
							@endif
						</tr>
						@foreach($specification->restMaterials as $material)
							<tr>
								<td> {{ date('d-m-Y', strtotime($material->date)) }} </td>
								<td> {{ $material->amount }} {{ $material->unit }} </td>
								<td> {{ $material->name }} </td>
								@if($options->showRate)
									<td> 
										<span style="font-family: tahoma !important"> &#0128; </span>  
										{{ number_format($material->price, 2, ',', '') }}
									</td>
									<td> 
										<span style="font-family: tahoma !important"> &#0128; </span> 
										{{ number_format($material->cost, 2, ',', '') }}
									</td>
								@endif
							</tr>
						@endforeach
						
						@if($options->showRate)
							<tr>
								<td colspan="3"></td>
								<td> <b> Totaal: </b> </td>
								<td> <b> <span style="font-family: tahoma !important"> &#0128; </span><!--
								-->{{ number_format(totalPrice($specification->restMaterials, false), 2, ',', '') }} </b> </td>
							</tr>
						@endif
					</table>		
				@endif	
				
				{{-- @if($options->totals && $options->showHours > 0) --}}
				@if($options->totals)
					<h2>Totalen</h2>
					<div style="width: 70%">
						<table class="table data-table">
							@if($options->showRate && $options->showMats)
								<tr>
									<td> Totale prijs materialen: </td>
									@if($options->showVatMat || !$options->applyVatMat)
										<td> 
											@if($options->showVatMat) 
												Excl. BTW: 
											@endif
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ number_format($total_price_materials, 2, ',', '') }} 
										</td> 
									@endif
									@if($options->showVatMat || $options->applyVatMat )
										<td>
											@if($options->showVatMat) Incl. BTW: @endif
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ number_format($total_price_materials * 1.21, 2, ',', '') }} 
										</td>
									@endif
								</tr>
							@endif
							@if($options->showHours > 1)
								<tr>
									<td> Totale prijs uren: </td>
									@if($options->showVatHours || !$options->applyVatHours)
										<td> 
											@if($options->showVatMat) 
												Excl. BTW: 
											@endif
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ number_format($total_of_hours->price, 2, ',', '') }} 
										</td>
									@endif
									@if($options->showVatHours || $options->applyVatHours)
										<td>
											@if($options->showVatHours) Incl. BTW: @endif
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ number_format($total_of_hours->price * 1.21, 2, ',', '') }} 
										</td>	
									@endif
								</tr>
							@endif
							@if($options->showRate && $options->showMats)
								<tr>
									<td> Totale prijs: </td>
									@if($options->showVatHours && $options->showVatMat)
										<td> 
											Excl. BTW:
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ number_format($total_price, 2, ',', '') }} 
										</td>
										<td>
											Incl. BTW:
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ number_format($total_price * 1.21, 2, ',', '') }} 
										</td>	
									@else
										<td>
											<span style="font-family: tahoma !important"> &#0128; </span><!--
											-->{{ totalPriceException($total_price_materials, $total_of_hours->price, $options) }}
										</td>
									@endif								
								</tr>
							@endif
							<tr>
								<td> Totaal aantal uren: </td>
								<td colspan="2"> {{ $total_of_hours->hours }} </td> 
							</tr>
						</table>
					</div>
				@endif
			@endif

			@include('pdf.photos')
		</div>
	</main>
</body>
</html>
