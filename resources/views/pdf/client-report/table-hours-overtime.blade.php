@if(includesHours($user->hours, true))
	<h4>Meerwerk</h4>
	<table class="table">
		<tr>
			@if($options->showHours > 0)
				<th style="width: 30px">Uren</th>
			@endif
			@if($options->showHours > 1)
				<th style="width: 120px">Meerwerk</th>
				<th style="width: 60px">Prijs</th>
			@endif
			<th>Omschrijving</th>
		</tr>
		@foreach($user->hours as $hour)
			@if($hour->overtime_rate_id)
				<tr>
					@if($options->showHours > 0)
						<td> {{ outputHours($hour->minutes) }} </td>
					@endif
					@if($options->showHours > 1)
						<td> {{ $hour->overtime_rate->name }} ({{ $hour->overtime_percentage }}%) </td>
						<td> 
							<span style="font-family: tahoma !important"> &#0128;</span><!--
							-->{{ number_format(($user->rate * ($hour->minutes / 60)) * (1 + ($hour->overtime_percentage / 100)), 2, ',', '') }} 
							{{-- {{ outputPriceHours($user->rate, $hour->minutes, $options->applyVatHours) }}  --}}
						</td>
					@endif
					<td> {{ $hour->description ? 
							$hour->description : 
							'Geen opmerking ingevoerd' }} 
					</td>
				</tr>
			@endif
		@endforeach
		@if($options->showHours > 0)
			<tr>
				<td> <b> {{ outputHours(totalHours($user->hours)) }} </b> </td>
				@if($options->showHours > 1)
					<td> <b> €{{ totalPriceHours($user->hours, $user->rate, $options->applyVatHours, 1) }} </b> </td>
				@endif
			</tr>
		@endif
	</table>
@endif
