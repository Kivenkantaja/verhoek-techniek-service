@if(includesHours($user->hours, false))
	<table class="table">
		<tr>
			@if($options->showHours > 0)
				<th style="width: 30px">Uren</th>
			@endif
			@if($options->showHours > 1)
				<th style="width: 60px">Prijs</th>
			@endif
			<th>Omschrijving</th>
		</tr>
		@foreach($user->hours as $hour)
			@if(!$hour->overtime_rate_id)
				<tr>
					@if($options->showHours > 0)
						<td> {{ outputHours($hour->minutes) }} </td>
					@endif
					@if($options->showHours > 1)
						<td> 
							<span style="font-family: tahoma !important"> &#0128;</span><!--
							-->{{ outputPriceHours($user->rate, $hour->minutes, $options->applyVatHours) }} 
						</td>
					@endif
					<td> {{ $hour->description ? 
							$hour->description : 
							'Geen opmerking ingevoerd' }} 
					</td>
				</tr>
			@endif
		@endforeach
		@if($options->showHours > 0)
			<tr>
				<td> <b> {{ outputHours(totalHours($user->hours)) }} </b> </td>
				@if($options->showHours > 1)
					<td> <b> €{{ totalPriceHours($user->hours, $user->rate, $options->applyVatHours, false) }} </b> </td>
				@endif
			</tr>
		@endif
	</table>
@endif
