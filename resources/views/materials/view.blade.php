@extends('layout')

@section('content')
	<h1>{{ $material->name }}</h1>

	<button type="button" v-on:click="previous()" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> </button>
	
	@if (user()->can('edit', \App\Material::class))
		<a href="/materialen/bewerken/{{ $material->id }}" class="btn btn-default">
			<i class="fa fa-pencil-alt"></i> 
		</a>
	@endif

	@if(count($projects) == 0 && user()->can('edit', \App\Material::class))
		<button type="button" v-on:click="confirmDelete = true;" class="btn btn-primary"> 
			<i class="fa fa-trash"></i> 
		</button>

		@if(isset($material))
			<modal v-on:close="confirmDelete = null" v-if="confirmDelete">
				<h4>Weet u het zeker?</h4>
				<button type="button" v-on:click="confirmDelete = false; showModal = false;" 
				class="btn btn-default"> Annuleren <i class="fa fa-arrow-left"></i> </button>
				<a class="btn btn-primary" href="/materialen/verwijderen/{{ $material->id }}">Doorgaan</a>	
			</modal>
		@endif		
	@else
		<h4 style="margin-top: 20px">Wordt gebruikt in:</h4>
		<ul>
			@foreach($projects as $displayProject)
				<li> <a href="/projecten/inzien/{{ $displayProject->id }}"> {{ $displayProject->name }} </a> </li>
			@endforeach
		</ul>
	@endif

	@if(isset($material))
		<table class="table mt-2">
			<thead>
				<th colspan="2">Gegevens</th>
			</thead>
			<tbody>
				<tr>
					<td>Omschrijving</td>
					<td>{{ $material->description }}</td>
				</tr>
				<tr>
					<td>Eenheid</td>
					<td>{{ $material->unit }}</td>
				</tr>
				<tr>
					<td>Artikelnummer</td>
					<td>{{ $material->number }}</td>
				</tr>
			</tbody>
		</table>
	@endif

	<upload-photo :can-edit="{{ user()->can('edit', \App\Material::class) ? 'true' : 'false' }}" 
				  data="{{ $material }}">
	</upload-photo>
@stop
