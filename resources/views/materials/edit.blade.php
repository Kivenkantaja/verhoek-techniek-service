@extends('layout')

@section('content')
	<h1> <a href="/materialen">Materialen</a>/nieuw</h1>
	<button type="button" v-on:click="previous()" class="btn btn-primary">Terug <i class="fa fa-arrow-left"></i> </button>

	<form action="/materialen/opslaan" method="POST" class="tab">
		{{ csrf_field() }}
		<div class="tab-title">
			<h3>Materiaal aanmaken</h3>
		</div>
		<div class="tab-content">
			@if(isset($material))
				<input type="hidden" value="{{ $material->id }}" name="id"> 
			@endif

			<form-input name="name"
						text="Naam"
						icon="fa-user"
						value="{{ isset($material) ? $material->name : null }}">
			</form-input>			

			<form-input name="unit"
						text="Eenheid (bijv. meter)"
						icon="fa-pencil-alt"
						value="{{ isset($material) ? $material->unit : null }}">
			</form-input>					

			<form-input name="number"
						text="Artikelnummer"
						icon="fa-list-ol"
						value="{{ isset($material) ? $material->number : null }}">
			</form-input>

			<form-input name="type_number"
						text="Typenummer"
						icon="fa-list-ol"
						value="{{ isset($material) ? $material->type_number : null }}">
			</form-input>

			<form-input name="ean_number"
						text="EAN-nummer"
						icon="fa-list-ol"
						value="{{ isset($material) ? $material->ean_number : null }}">
			</form-input>

			<form-input name="price"
						text="Prijs"
						icon="fa-credit-card"
						value="{{ isset($material) ? $material->price : null }}">
			</form-input>

			<form-input name="buyPrice" 
						text="Inkoopprijs"
						icon="fa-credit-card"
						value="{{ isset($material) ? $material->buyPrice : null }}">
			</form-input>

			<form-input name="description" 
						text="Omschrijving (komt niet op rapport)"
						icon="fa-pencil-alt"
						value="{{ isset($material) ? $material->description : null }}">
			</form-input>

			<form-input name="minimum_stock"
						text="Minimum voorraad"
						icon="fa-cogs"
						value="{{ isset($material) ? $material->minimum_stock : null }}">
			</form-input>

			<div class="form-item no-margin">
				<div class="form-icon"></div>
				<div class="form-input form-input-1">
					<a href="/materialen" class="btn btn-default btn-block"> Annuleren <i class="fa fa-arrow-left"></i> </a>
				</div>

				<div class="form-input form-input-2">
					<button class="btn btn-primary btn-block"> Opslaan <i class="fa fa-save"></i> </button>
				</div>
			</div>

		</div>
	</form>		
@stop
