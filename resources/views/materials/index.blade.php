@extends('layout')

@section('content')
	<materials :can-edit="{{ user()->can('edit', \App\Material::class) ? 'true' : 'false' }}" 
			   v-on:updated="message = $event">
	</materials>
@stop
