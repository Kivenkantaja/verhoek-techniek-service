<div class="table-hours" v-if="data.showHours && displayHours && displayHours.project">
	<h4>Uren gemaakt op @{{ data.displayDate }} voor @{{ displayHours.project.name }} </h4>
	<button v-on:click="hourForm(null, data.date, displayHours.project.id)" 
			type="button" class="add-hours btn btn-primary"> 
		<i class="fa fa-plus"></i> 
	</button>
	<table class="table">
		<tr>
			<th> <i class="fa fa-hourglass"></i> </th>
			<th> <i class="fa fa-user"></i> </th>
			<th> <i class="fa fa-comment"></i> </th>	
			<th></th>		
		</tr>

		<tr v-for="hour in displayHours.hours">
			<td> @{{ hour.output }} </td>
			<td> @{{ hour.user.name }} </td>
			<td> @{{ hour.description ? hour.description : 'Geen omschrijving ingevoerd' }} </td>
			<th> 			
				<button v-if="confirmDelete != hour.id" v-on:click="confirmDelete = hour.id" class="delete-button"> 
					<i class="fa fa-trash"></i> 
				</button> 
				<button v-if="mode == 1 || !checkDeadline(hour.date)" 
						class="edit-button" 
						v-on:click="hourForm(null, null, null, hour.id)"> 
					<i class="fa fa-pencil-alt"></i> 
				</button> 					
				<div style="float: right" v-if="confirmDelete == hour.id">
					<button class="btn btn-primary" v-on:click="confirmDelete = false">Annuleren</button>
					<button class="btn btn-danger" v-on:click="deleteHour(hour.id)">Bevestigen</button>
				</div>
			</th>
		</tr>
	</table>
</div>
