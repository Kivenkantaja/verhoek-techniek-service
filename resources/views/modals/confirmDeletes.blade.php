@if(isset($project))
	<div v-if="confirmDelete">
		<h4>Weet u het zeker?</h4>
		<button type="button" v-on:click="confirmDelete = false; showModal = false;" 
		class="btn btn-default"> Annuleren <i class="fa fa-arrow-left"></i> </button>
		<a class="btn btn-primary" href="/projecten/verwijderen/{{ $project->id }}">Doorgaan</a>	
	</div>
@endif

@if(isset($client))
	<div v-if="confirmDelete">
		<h4>Weet u het zeker?</h4>
		<button type="button" v-on:click="confirmDelete = false; showModal = false;" 
		class="btn btn-default"> Annuleren <i class="fa fa-arrow-left"></i> </button>
		<a class="btn btn-primary" href="/klanten/verwijderen/{{ $client->id }}">Doorgaan</a>	
	</div>
@endif

@if(isset($material))
	<div v-if="confirmDelete">
		<h4>Weet u het zeker?</h4>
		<button type="button" v-on:click="confirmDelete = false; showModal = false;" 
		class="btn btn-default"> Annuleren <i class="fa fa-arrow-left"></i> </button>
		<a class="btn btn-primary" href="/materialen/verwijderen/{{ $material->id }}">Doorgaan</a>	
	</div>
@endif