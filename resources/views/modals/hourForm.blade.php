<div>	
	<form v-if="!confirmDelete" @submit.prevent="saveHours({{ Auth::user()->id }})">
		{{ csrf_field() }}

		<h3>Uren invullen</h3>
		
		<div v-if="page == 3 || user.user_type == 1">
			<div class="form-item">
				<div style="vertical-align: top" class="form-icon">
					<i class="fa fa-pencil-alt"></i>
				</div>
				<div class="form-input">
					<textarea-autosize 
						v-model="formData.description" 
						:min-height="30"
						:max-height="350"
					  	placeholder="Omschrijving" 
					  	class="form-control"
					/>						
				</div>				
			</div>
		</div>	
		
		<div v-if="page == 0 || user.user_type == 1">
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-search"></i>
				</div>
				<div class="form-input">
					<input type="text" class="form-control" placeholder="Zoeken" v-model="project_search">
				</div>
			</div>
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-tasks"></i>
				</div>
				<div class="form-input">
					<select class="form-control" v-model="formData.project_id">
						<option disabled selected value="0">Selecteer een project</option>
						<option v-for="project in projects.open" :value="project.id">
							@{{ project.client ? project.client.name : 'Verwijderde klant' }} - @{{ project.name }}
						</option>
					</select>
				</div>				
			</div>
		</div>

		<div v-if="page == 1 || user.user_type == 1">
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-calendar"></i>
				</div>
				<div class="form-input form-input-1">
					<datepicker :format="dateFormat"
								:disabled-dates="{ to: new Date(minDate - 8640000) }" 
								v-model="formData.date" 
								input-class="form-control datepicker"
								:language="nl"></datepicker>
				</div>	
				<div class="form-input form-input-2">
					<button type="button" v-on:click="formData.date = null" style="padding: 5px" class="btn-default btn-block">
						Geen datum
					</button>
				</div>			
			</div>
		</div>

		<div v-if="page == 2 || user.user_type == 1">
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-hourglass-half"></i>
				</div>
				<div class="form-input form-input-1">
					<input type="text" class="form-control" placeholder="Uren" v-model="formData.hours">
				</div>
				<div class="form-input form-input-2">
					<input type="text" class="form-control" placeholder="Minuten" v-model="formData.minutes">
				</div>				
			</div>	
		</div>		

		@if(Auth::user()->user_type == 1)
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-user"></i>
				</div>
				<div class="form-input">
					<select required v-model="formData.user_id" class="form-control">
						<option v-for="user in users.active" :value="user.id">@{{ user.name }}</option>
						<option v-for="user in users.disabled" :value="user.id">@{{ user.name }}</option>
					</select>
				</div>				
			</div>	
		@endif
		
		<div v-if="user.user_type == 0" class="form-item">
			<div class="form-icon"></div>
			<div class="form-input">
				<div class="pull-right">
					<button type="button" v-if="page > 0" v-on:click="page--" class="btn btn-primary"> < </button>
					<button type="button" v-if="page < 3" v-on:click="page++" class="btn btn-primary"> > </button>
				</div>
			</div>
		</div>
		
		<div class="form-item no-margin">
			<div class="form-icon">
				<input type="checkbox" v-model="formData.openMaterialForm" id="openMaterialForm">
			</div>
			<div class="form-input">
				<label class="unselectable" for="openMaterialForm">Materiaal invoeren na opslaan</label>
			</div>
		</div>

		<div class="form-item no-margin">
			<div class="form-icon"></div>
			<div class="form-input form-input-1">
				<button type="button" class="btn btn-default btn-block" 
						v-on:click="showModal = false; data = {}; confirmDelete = false; displayHours = {};"> 
					Annuleren <i class="fa fa-arrow-left"></i> 
				</button>
			</div>

			<div class="form-input form-input-2">
				<button class="btn btn-primary btn-block"> Opslaan <i class="fa fa-save"></i> </button>
			</div>
		</div>	
	</form>
</div>
