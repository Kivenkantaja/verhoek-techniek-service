@extends('layout')

@section('content')
	<h1> <a href="/projecten">Projecten</a>/bewerken/{{ $project->name }} </h1>
	<button type="button" v-on:click="previous()" class="btn btn-primary">Terug <i class="fa fa-arrow-left"></i> </button>

	<form action="/projecten/opslaan" method="POST" class="tab">
		{{ csrf_field() }}

		<input type="hidden" name="id" value="{{ $project->id }}">
		<div class="tab-title">
			<h3>Project bewerken</h3>
		</div>
		<div class="tab-content">
			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-user"></i>
				</div>
				<div class="form-input">
					<select class="form-control" name="client_id">
						@foreach($clients as $client)
							<option {{ $project->client_id == $client->id ? "selected" : null }} 
									value="{{ $client->id }}"> {{ $client->name }} </option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-item">
				<div class="form-icon">
					<i class="fa fa-pencil-alt"></i>
				</div>
				<div class="form-input">
					<input value="{{ $project->name }}" type="text" class="form-control" name="name" placeholder="Naam">
				</div>
			</div>

			<input type="hidden" value="{{ Auth::user()->id }}" name="user_id">

			<div class="form-item no-margin">
				<div class="form-icon"></div>
				<div class="form-input form-input-1">
					<a href="/projecten" class="btn btn-default btn-block"> Annuleren <i class="fa fa-arrow-left"></i> </a>
				</div>

				<div class="form-input form-input-2">
					<button class="btn btn-primary btn-block"> Opslaan <i class="fa fa-save"></i> </button>
				</div>
			</div>

		</div>
	</form>	
@stop
