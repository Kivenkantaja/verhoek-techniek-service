@extends('layout')

@section('content')
	<div class="d-flex">
		<h1 class="flex-grow-1">{{ $project->name }}</h1>

		<div>
			<button type="button" v-on:click="previous()" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> </button>

			@if(user()->user_type == 1)
				<a href="/projecten/bewerken/{{ $project->id }}" class="btn btn-default">
					<i class="fa fa-edit"> </i>
				</a>

				<button type="button" v-on:click="confirmDelete = true; showModal = true" class="btn btn-primary">
					<i class="fa fa-trash-alt"></i>
				</button>
			@endif
		</div>
	</div>
	
	<project-show :project="{{ $project }}" :user="{{ user() }}"></project-show>
@stop
