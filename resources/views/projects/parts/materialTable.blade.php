<td> 
	@{{ material.date }}
</td>
<td>
	@{{ material.amount }} @{{ material.unit }} <br>
	<div v-if="material.alt">
		@{{ material.name }}
	</div>
	<div v-else>
		<a :href="'/materialen/inzien/'+material.id">@{{ material.name }} </a> 
	</div>
</td>							
<td>
	<div v-if="material.alt">
		Eenmalig
	</div>
	<div v-else>
		@{{ material.number }} 
	</div>
	<span v-if="material.price">€@{{ material.price }}</span> 
</td>
<td>
	<button v-on:click="unlinkMaterial(material.link_id)" 
			style="float: none; color: red" 
			class="delete-button"> 
		<i class="fa fa-trash"></i> 
	</button>							
</td>