@extends('layout')

@section('content')
	<div>
		<h1>Projecten</h1>
		<a href="/projecten/nieuw" class="btn btn-primary">
			Nieuw project <i style="margin-left: 5px;" class="fa fa-plus"></i> 
		</a>
		
		<project-list></projects-list>		
	</div>
@stop