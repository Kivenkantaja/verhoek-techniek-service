<div class="modal-overlay" v-if="showModal != false">
	<div class="modal-content modal-form">
		 
		<div class="modal-close">
			<a href="#" @click.prevent="closeModal()"> 
				<i class="fa fa-times"></i> 
			</a>
		</div>

		@include('modals.hourForm')
		@include('modals.confirmDeletes')
		@include('modals.displayHours')
	</div>
</div>