<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Verhoek Techniek Service</title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="manifest" href="/manifest.json">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="#">
</head>
<body>
	<div id="app" v-cloak>
		<div class="navbar">
			<div class="pull-left">
				<div class="navbar-item">
					<span style="margin-left: 20px" v-on:click="showNav = !showNav" class="hamburger">
						<i class="fa fa-bars"></i>
					</span>
				</div>					
				<div class="navbar-item">
					<img src="/images/logo.png" alt="VTS Logo" class="nav-logo">					
				</div>
			</div>
			
			<div class="pull-right">
				<div class="navbar-item">
					<p class="navbar-text">Welkom, {{ Auth::user()->name }}</p>
				</div>

				<div class="navbar-item">
					<a href="/loguit" class="btn btn-edit btn-navbar">
						<i class="fas fa-sign-out-alt"></i>
					</a>
				</div>					
			</div>
		</div>

		<div :class="{'sidebar-mobile': showNav}" class="sidebar">
			<div class="profile">
				<button v-on:click="showProfile = true" class="profile-button">
					{{ user()->name[0] }}
				</button> <br>

				<b>{{ user()->name }}</b> <br>
				{{ user()->user_type == 1 ? 'Administrator' : 'Gebruiker' }}
			</div>

			@if (user()->user_type === 1) <nav-item link="/" icon="fa-home">Home</nav-item> @endif

			<nav-item link="/projecten" icon="fa-tasks">Projecten</nav-item>
			<nav-item link="/uren" icon="fa-clock">Uren</nav-item>
			<nav-item link="/timernotes" icon="fa-pencil-alt">Notities</nav-item>
			
			@if (user()->user_type === 1) <nav-item link="/klanten" icon="fa-user">Klanten</nav-item> @endif
			@if (user()->can('see', \App\Material::class)) <nav-item link="/materialen" icon="fa-tools">Materialen</nav-item> @endif
			
			@if (user()->user_type === 1)
				<nav-item link="/gebruikers" icon="fa-users">Gebruikers</nav-item>
				<nav-item link="/feedback" icon="fa-user">Feedback</nav-item>
				<nav-item link="/costs" icon="fa-credit-card">Kosten</nav-item>
			@endif
		</div>
		
		<div class="content">
			<div v-if="message != null" 
				 class="alert col-md-3" 
				 :class="{'alert-success': message.status == 1, 'alert-danger': message.status == 0}"
				 role="alert"> 
				@{{ message.message }} 
			</div>
			@yield('content')

			<hour-form v-if="hours_form"
					   user_id="{{ Auth::user()->id }}" 
					   v-on:saved="handleSavedHours($event)"
					   v-on:error="handleError($event)"
					   v-on:close="hours_form = null"
					   :data="hours_form">		   	
			</hour-form>
		
			@if(Auth::user()->user_type == 1)		
				<profile v-if="showProfile"
						 v-on:close="showProfile = false"
						 :user="user">
				</profile>
			@endif
		</div>

		<button type="button" 
				style="position: fixed; right: 10px; bottom: 10px;" 
				class="btn btn-default"
				v-on:click="hours_form = {}"> 
			<i class="fa fa-plus"></i> 
		</button>

		@include('modal')
	</div>

	<script src="/js/app.js"></script>
</body>
</html>
