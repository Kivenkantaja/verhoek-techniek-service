@extends('layout')

@section('content')
	<h1>Gebruikers</h1>
	<a href="/gebruikers/nieuw" class="btn btn-primary"> Nieuwe gebruiker <i class="fa fa-plus"></i> </a>

	<users status="active"></users>
	<users status="disabled"></users>
@stop