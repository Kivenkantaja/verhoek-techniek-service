@extends('layout')

@section('content')
	@if(!isset($user))
		<h1>Gebruikers/aanmaken</h1>
	@else
		<h1>Gebruikers/bewerken/{{ $user->name }}</h1>
	@endif

	<form class="tab" action="/gebruikers/opslaan" method="POST">
		{{ csrf_field() }}
		@if(isset($user)) <input type="hidden" value="{{ $user->id }}" name="id"> @endif
		<div class="tab-title">
			<h3>Gebruiker aanmaken</h3>
		</div>
		<div class="tab-content">
			<form-input name="name" 
						text="Naam" 
						icon="fa-user" 
						value="{{ isset($user) ? $user->name : null }}">	
			</form-input>
			
			<form-input name="email" 
						text="E-mailadres" 
						icon="fa-envelope" 
						value="{{ isset($user) ? $user->email : null }}">	
			</form-input>

			<div class="form-item no-margin">
				<div class="form-icon"></div>
				<div class="form-input form-input-1">
					<a href="/klanten" class="btn btn-default btn-block"> Annuleren <i class="fa fa-arrow-left"></i> </a>
				</div>

				<div class="form-input form-input-2">
					<button class="btn btn-primary btn-block"> Opslaan <i class="fa fa-save"></i> </button>
				</div>
			</div>			
		</div>
	</form>
@stop