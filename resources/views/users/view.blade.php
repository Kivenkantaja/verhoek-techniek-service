@extends('layout')

@section('content')
	<h1>Gebruikers/inzien/{{ $user->name }}</h1>

	<button type="button" v-on:click="previous()" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> </button>
	<a href="/gebruikers/bewerken/{{ $user->id }}" class="btn btn-default"> <i class="fa fa-pencil-alt"> </i> </a>	
	
	<div class="align-with-table">
		<a href="/toggleUser/{{ $user->id }}" class="btn btn-primary no-left-margin"> 
			{{ $user->status == 0 ? 'Activeren' : 'Deactiveren' }} 
		</a>
	</div>
	<table class="table table data-table info-table">
		<thead>
			<th colspan="2">Gegevens</th>
		</thead>
		<tr>
			<td> <b>Naam</b> </td>
			<td> {{ $user->name }} </td>
		</tr>
		<tr>
			<td> <b>E-mail</b> </td>
			<td> {{ $user->email }} </td>
		</tr>
		<tr>
			<td> <b>Tarief</b> </td>
			<td> €{{ $user->rate ? $user->rate : '0,00' }} </td>
		</tr>
	</table>	

	<activities data="{{ $user }}"></activities>
@stop
