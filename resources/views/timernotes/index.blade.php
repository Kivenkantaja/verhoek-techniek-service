@extends('layout')

@section('content')
	<div>
		<timer-notes v-on:message="message = $event" :user="user"></timer-notes>
	</div>
@stop