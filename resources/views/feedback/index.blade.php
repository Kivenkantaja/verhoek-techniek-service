@extends('layout')

@section('content')
	<h1>Feedback</h1>

	<feedback status="open" v-on:message="message = $event; $refs['closedFeedback'].load()"></feedback>
	<feedback status="done" v-on:message="message = $event;" ref="closedFeedback"></feedback>
@stop